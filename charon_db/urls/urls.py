from charon_db.views import *
from django.urls import path
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('list_guide/', ListGuide.as_view(),name="list_guide"), #anu
    path('test/', TestWyView.as_view(),name="test"), #anu
    path('list_token/', ListTokenList.as_view(),name="list_token"), #anu
    path('create_guide/', login_required(CreateGuide.as_view()),name="create_guide"),
    path('delete_guide/<int:pk>', login_required(DeleteGuide.as_view()),name="delete_guide"),
    path('edit_guide/<int:pk>', login_required(EditGuide.as_view()),name="edit_guide"),
    path('edit_com_part/<int:pk>', login_required(EditComPart.as_view()),name="edit_com_part"),
    path('edit_part/<int:pk>', login_required(EditPart.as_view()),name="edit_part"),
    path('rest_part_type/', login_required(RestCommPartsView.as_view()),name="part_type_rest"),
    path('list_part_com/', login_required(ListGeneralPart.as_view()),name="list_part_com"),
    path('delete_part_com/<int:pk>', login_required(DeletePartCom.as_view()),name="delete_part_com"),
    path('delete_part/<int:pk>', login_required(ListGeneralPart.as_view()),name="delete_part"),
    path('create_part_com/', login_required(CreateComPart.as_view()),name="create_com_part"),
    path('list_mod/', login_required(ListModStructView.as_view()),name="list_mod"),#anu
    path('proxy_filiere/', proxy_filiere.as_view(),name="proxy_filiere"),
    path('proxy_etape/', proxy_etape.as_view(),name="proxy_etape"),
    path('proxy_etapes/', proxy_etapes.as_view(),name="proxy_etapes"),
    path('proxy_diplome_info/', proxy_diplome_info.as_view(),name="proxy_diplome_info"),
    path('proxy_diplome_t/', proxy_diplome_t.as_view(),name="proxy_diplome_t"),
    path('proxy_filiere_info/', proxy_filiere_info.as_view(),name="proxy_filiere_info"),
    path('proxy_etp_tree/', proxy_etp_tree.as_view(),name="proxy_etp_tree"),
    path('edit_module/<str:code>', login_required(EditModuleView.as_view()),name="edit_module"),#anu
    path('update_mod/<str:code>', login_required(MinimalMod.as_view()),name="mini_mod"),#anu
    path('list_part_gui/', login_required(ListSpecificPart.as_view()),name="list_part_gui"),
    path('general_cal/', login_required(ListCalendar.as_view()),name="list_part_gui"), #anu
    path('add_event_calendar/', login_required(ModalCalendarAddEvent.as_view()), name='add_event'),
    path('add_token/', login_required(ModalToken.as_view()), name='add_token'),
    path('module_calendar/', login_required(EditCalendarStructureModule.as_view()), name='module_calendar'),
    path('edit_module_calendar/<str:code>', login_required(ModuleCalendarUpdateDate.as_view()), name='module_calendar_edit'),
    path('list_couverture/', login_required(ListCouverture.as_view()), name='list_couv'),#anu
    path('edit_couverture/<int:pk>', login_required(CouvertureUpdate.as_view()), name='edit_couv'),
    path('generator/<int:pk>', login_required(GuideGeneratorView.as_view()), name='generator'),
    path('generator_test/<int:pk>', login_required(GuideGeneratorTestView.as_view()), name='generator_test'),
    path('list_guide_gen/', login_required(ListGuideGen.as_view()), name='list_for_gen'),
    path('list_histo/', login_required(ListGuideHistorique.as_view()), name='list_histo'),
    path('delete_general_calendar/<int:pk>', login_required(DeleteCalendarGeneralView.as_view()), name='delete_gen'),
    path('delete_token/<int:pk>', login_required(DeleteToken.as_view()), name='delete_token'),
    path('edit_token/<int:pk>', login_required(ModalUpdateToken.as_view()), name='edit_token'),
    path('edit_event/<int:pk>', login_required(ModalCalendarUpdateEvent.as_view()), name='edit_event'),
    path('rest_module_has_date/<str:code>', login_required(RestModuleHasDate.as_view()), name='rest_module_has_date'),
    path('copy/', login_required(YearCopy.as_view()), name='copy'),
    path('mod_rest/<str:code>', RestModule.as_view(), name='rest-module'),
    path('n_calendar_rest/<str:code>', DateModelForm.as_view(), name='n_calendar_rest'),
    path('update_calendar_modal/', UpdateCalendarModale.as_view(), name='update_calendar_modale'),

]