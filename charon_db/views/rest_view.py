from django.views import View
from django.views.generic import TemplateView
from charon_db.models import *
from django.http import JsonResponse,HttpResponse
import requests
from django.conf import settings
import json
from django.forms.models import model_to_dict
from charon_db.forms import ModuleCalendarUpdateDateForm
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from datetime import datetime
"""

Return T if the module has a date F if not

"""

class RestModuleHasDate(View):

    def get(self,*args,**kwargs):

        code = self.kwargs['code']
        try:
            mod = Module.objects.get(code=code,annee_module=Annee.objects.get(current=True))
            if mod.date_debut_examen and mod.date_fin_examen and mod.date_debut_examen_r and mod.date_fin_examen_r:

                return JsonResponse({'res':'T'})

            else:

                return JsonResponse({'res':'F'})

        except Exception:

            return JsonResponse({'res':'F'})
"""

Return a rest list of part of type sent a get parameters (type)

"""
class RestCommPartsView(View):

    def get(self,*args,**kwargs):

        type = self.request.GET.get('type')
        parts = Parts.objects.filter(type_parti=type,annee_guide=Annee.objects.get(current=True))
        json = parts.values('titre','id')

        return JsonResponse(list(json),safe=False)


"""

Proxy Filiere request 

"""
class proxy_filiere(View):

    params = []
    url = settings.FILIERE
    annualised = True

    def set_param(self):

        res = ""

        for v in self.params:

            value = self.request.GET.get(v)
            res+='/'+str(value)

        if self.annualised:

            an = Annee.objects.get(current=True)
            res+='/'+str(an.cod_anu)

        return res[1:]

    def get(self,*args,**kwargs):

        params = self.set_param()

        try:
            print(self.url+params)

            req = requests.get(url=self.url+params)

            res = req.json()

            return HttpResponse(json.dumps(res), content_type='application/json')

        except Exception as e:
            print(e)
            return HttpResponse([])

"""

Proxy Diplome_tree

"""
class proxy_diplome_t(proxy_filiere):


    params = ['code']
    annualised = True
    url = settings.DIPLOME_T

"""

Proxy Diplome info

"""
class proxy_diplome_info(proxy_filiere):

    annualised = True
    params = ['code','version']
    url = settings.DIPLOME

"""

Proxy ETAPE request

"""

class proxy_etape(proxy_filiere):

    url = settings.ETAPE
    annualised = True
    params = ['code','version']


class proxy_filiere_info(proxy_filiere):

    url = settings.FILIERE_INFO
    annualised = False
    params = ['id']

class proxy_etp_tree(proxy_filiere):

    url = settings.TREE
    annualised = True
    params = ['code','version']

class proxy_etapes(proxy_filiere):

    annualised = True
    url = settings.ETAPES
    params = ''

class RestModule(View):

    def get(self,*args,**kwargs):

        code = self.kwargs['code']

        try:
            mod = Module.objects.get(code=code,annee_module=Annee.objects.get(current=True))
            dict = model_to_dict(mod)

            return JsonResponse(dict)

        except Exception:

            return JsonResponse({'err':"Erreur module inconnu"})

class DateModelForm(TemplateView):

    template_name = 'minimal_form_select.html'

    def get_context_data(self, **kwargs):

        ctx = super(DateModelForm,self).get_context_data(**kwargs)

        code = self.kwargs['code']
        module = Module.objects.get(code=code,annee_module=Annee.objects.get(current=True))

        ctx['form'] = ModuleCalendarUpdateDateForm(instance=module)

        return ctx

@method_decorator(csrf_exempt, name='dispatch')
class UpdateCalendarModale(View):

    def str_to_boolean(self,val):

        if val == 'false':

            return False

        elif val == 'true':

            return True

    def todatetime(self,val):

        if val =='':

            return None

        res =  datetime.strptime(val, '%d-%m-%Y à %H:%M')

        return res

    def post(self,*args,**kwargs):


        try:

            mod = Module.objects.get(code=self.request.POST.get('id'),annee_module=Annee.objects.get(current=True))
            mod.date_debut_examen = self.todatetime(self.request.POST.get('debut'))
            mod.date_fin_examen = self.todatetime(self.request.POST.get('fin'))
            mod.salle_examen = self.request.POST.get('salle')
            mod.date_debut_examen_r = self.todatetime(self.request.POST.get('debutr'))
            mod.date_fin_examen_r = self.todatetime(self.request.POST.get('finr'))
            mod.salle_examen_r = self.request.POST.get('saller')

            mod.examen_periode1 = self.str_to_boolean(self.request.POST.get('p1'))
            mod.examen_periode2 =  self.str_to_boolean(self.request.POST.get('p2'))


            mod.save()

        except Exception as e:

            print(e)
            return HttpResponse('NOK')

        return HttpResponse('OK')

