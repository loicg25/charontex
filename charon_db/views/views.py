from django.views.generic.edit import FormView,DeleteView,UpdateView,CreateView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView,View
from charon_db.forms import *
from charon_db.models import *
from django.http import HttpResponseRedirect
from django.shortcuts import render
import django_filters
from django_filters.views import FilterView
from bootstrap_modal_forms.generic import BSModalCreateView,BSModalUpdateView
from django.http import HttpResponse
from charon_libtex.lib import *
from urllib.parse import quote
import datetime
import requests
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db import transaction
from django.core.files import File

from charon_libtex.lib.generator import GuideGeneratorTex

"""

Permission that permit acces to super user  

"""
class IsSuperUserPermissionMixin(UserPassesTestMixin):

    def test_func(self):


        if self.request.user.is_superuser:

            return True


        return False

    #définie ce sue l'utilisateur verra si il n'est pas authorise
    def handle_no_permission(self):

        return HttpResponse('Vous n\'êtes pas autorisé à modifier cet élément')



class TestWyView(TemplateView):

    template_name = "watermark.html"


"""

View that list all the guide available

"""
class ListGuide(IsSuperUserPermissionMixin,ListView):

    model = Guide
    paginate_by = 15
    template_name = "list_guide.html"

    def get_queryset(self):

        queryset = Guide.objects.filter(annee_guide=Annee.objects.get(current=True)).order_by('intitule')

        return queryset

"""

View that list all the guide available

"""
class ListGuideGen(ListView):

    model = Guide
    paginate_by = 15
    template_name = "list_guide_gen.html"

    def get_queryset(self):

        queryset = Guide.objects.filter(annee_guide=Annee.objects.get(current=True)).order_by('intitule')

        return queryset

"""

View that list all the guide available

"""
class ListGuideHistorique(IsSuperUserPermissionMixin,ListView):

    model = Guide
    paginate_by = 15
    template_name = "list_historique.html"

    def get_queryset(self):

        queryset = Guide.objects.filter(annee_guide=Annee.objects.get(current=True)).order_by('intitule')

        return queryset

"""

View That permit to create a guide from scratch

"""
class CreateGuide(IsSuperUserPermissionMixin,TemplateView):

    template_name = 'charon_db/create_guide_form.html'
    success_url = '/db/list_guide/'

    def parse_com(self,data):

        s_data = data.split(':')

        if s_data[1] != '*':
            s_type = s_data[1]
        else:
            s_type = None

        if s_data[3] != '*':
            s_p = s_data[3]
        else:
            s_p = None

        return {'titre':s_data[2],'type':s_data[0],'s_type':s_type,'parametre':s_p}

    def post(self, request, *args, **kwargs):

        annee = Annee.objects.get(current=True)
        guide = Guide(intitule=request.POST.get('title_guide'),
                      annee_guide=annee)
        n_order = 0
        #we first create the guide
        guide.save()
        #then each part giving them an order according their position in POST data array

        for part in request.POST.getlist('parts[]'):

            t_part = self.parse_com(part)

            p = Parts(titre=t_part['titre'],type_parti=t_part['type'],sous_type=t_part['s_type'],parametre=t_part['parametre'],annee_guide=Annee.objects.get(current=True))

            #we create the page if the part doesnt exist

            if t_part['type'] == 'gui' or t_part['type'] == 'com':

                page = Page()
                page.titre = p.titre
                page.save()
                p.page = page

            p.save()
            #we add part to guide
            guide.parti.add(p)
            op = OrdrePartiGuide(numero_ordre=n_order,guide=guide,partie=p)
            op.save()
            #after we go to next part
            n_order +=1

        return HttpResponseRedirect(self.success_url)


"""

View That permit to edit a guide from scratch

"""
class EditGuide(IsSuperUserPermissionMixin,TemplateView):

    template_name = 'charon_db/edit_guide_form.html'
    success_url = '/db/list_guide/'

    def get_context_data(self, **kwargs):

        ctx = super(EditGuide,self).get_context_data(**kwargs)
        pk_guide = kwargs['pk']
        guide = Guide.objects.get(id=pk_guide)
        ctx['guide'] = guide
        self.request.session['prev_url'] = '/db/edit_guide/'+str(guide.id)
        ctx['parti_ordonnee'] = sorted(guide.parti.all(),key=lambda p:p.ordrepartiguide_set.filter(guide=guide).last().numero_ordre)

        return ctx

    def parse_com(self,data):

        s_data = data.split(':')

        if s_data[1] != '*':
            s_type = s_data[1]
        else:
            s_type = None

        if s_data[3] != '*':
            s_p = s_data[3]
        else:
            s_p = None

        try:

            if s_data[4]:

                id = s_data[4]

            else:

                id = None

        except:

            id = None

        return {'titre':s_data[2],'type':s_data[0],'s_type':s_type,'parametre':s_p,'id':id}

    def post(self, request, *args, **kwargs):

        guide = Guide.objects.get(id=request.POST.get('guide_id'))
        guide.intitule = request.POST.get('title_guide')
        guide.prefixe = request.POST.get('prefixe')

        n_order = 0
        #we first create the guide
        guide.save()

        #then each part giving them an order according their position in POST data array
        keep_id = []

        for part in request.POST.getlist('parts[]'):

            #we parse data of each tab entry
            t_part = self.parse_com(part)

            #then we edit the different part, create new part and finaly delete part not present anymore

            if t_part['id']: #their is an id we edit it

                p = Parts.objects.get(id=t_part['id'])
                keep_id.append(p.id)
                p.save()

                try:

                    op = OrdrePartiGuide.objects.get(guide=guide,partie=p)
                    op.delete()

                except Exception as e:

                    pass

                nop = OrdrePartiGuide(numero_ordre=n_order,guide=guide,partie=p)
                nop.save()

            else: #no id we create it

                #or add non existing part
                p = Parts(titre=t_part['titre'], type_parti=t_part['type'], sous_type=t_part['s_type'],parametre=t_part['parametre'],annee_guide=Annee.objects.get(current=True))
                #if we need a page we create and linked it

                if t_part['type'] == 'gui' or t_part['type'] == 'com':

                    page = Page()
                    page.titre = p.titre
                    page.save()
                    p.page = page

                p.save()
                # we add part to guide
                guide.parti.add(p)
                op = OrdrePartiGuide(numero_ordre=n_order, guide=guide, partie=p)
                op.save()
                keep_id.append(p.id)

            #after we go to next part
            n_order +=1

        #we delete part not needed anymore
        Parts.objects.filter(guide=guide,type_parti='gui').exclude(id__in=keep_id).delete() #first part specific to a guide not need anymore we simply totaly delete
        Parts.objects.filter(guide=guide,type_parti='gen').exclude(id__in=keep_id).delete() #first part specific to a guide not need anymore we simply totaly delete
        parti_com = Parts.objects.filter(guide=guide,type_parti='com').exclude(id__in=keep_id) #then part not specific to a guide we juste remove link

        for r_part_com in parti_com: #remove link  but not the part as it can be used elsewhere
            guide.parti.remove(r_part_com)

        for id in keep_id:
            try:

                guide.parti.add(Parts.objects.get(type_parti='com',id=id))

            except Parts.DoesNotExist:

                pass

        OrdrePartiGuide.objects.filter(guide=guide).exclude(partie_id__in=keep_id).delete()

        return HttpResponseRedirect(self.success_url)


"""

Permit to create a com part

"""
class CreateComPart(IsSuperUserPermissionMixin,TemplateView):

    template_name = "create_com_form.html"


    def get_context_data(self, **kwargs):

        ctx = super(CreateComPart,self).get_context_data(**kwargs)
        ctx['part'] = PartComForm
        ctx['page'] = PageForm

        return ctx

    def post(self,*args,**kwargs):

        titre = self.request.POST.get('titre')
        contenu_page = self.request.POST.get('contenu_page')

        pg = Page()
        pg.titre = titre
        pg.contenu_page = contenu_page
        pg.save()

        p = Parts()
        p.annee_guide = Annee.objects.get(current=True)
        p.titre = titre
        p.type_parti = 'com'
        p.save()
        p.page = pg
        p.save()

        return HttpResponseRedirect('/db/list_part_com/')



"""

View that permit to edit a part and go back to previous guide

"""

class EditComPart(TemplateView):

    # model = Parts
    template_name = 'charon_db/parts_form.html'
    success_url = '/db/list_part_com/'
    # fields = ['titre','type_parti','sous_type','parametre','page']

    def get_context_data(self, **kwargs):

        ctx = super(EditComPart,self).get_context_data(**kwargs)
        my_part = Parts.objects.get(id=kwargs['pk'])
        ctx['form_part'] = PartComForm(instance=my_part)
        ctx['form_page'] = PageForm(instance=my_part.page)

        return ctx


    def post(self, request, *args, **kwargs):

        if 'rollback' in self.request.POST:

            prev_url = self.request.session['prev_url']

            return HttpResponseRedirect(prev_url)

        else:

            my_part = Parts.objects.get(id=kwargs['pk'])
            form_com = PartComForm(request.POST,instance=my_part)
            form_page = PageForm(request.POST,instance=my_part.page)

            if form_page.is_valid() and form_com.is_valid():

                form_com.save()
                form_page.save()

                return HttpResponseRedirect(self.success_url)

            else:

                return render(request, self.template_name, {'form_part': form_com, 'form_page': form_page}, )


class EditPart(TemplateView,):

    # model = Parts
    template_name = 'charon_db/parts_form.html'
    success_url = '/db/list_part_gui/'
    # fields = ['titre','type_parti','sous_type','parametre','page']

    def get_context_data(self, **kwargs):

        ctx = super(EditPart,self).get_context_data(**kwargs)
        my_part = Parts.objects.get(id=kwargs['pk'])
        ctx['form_part'] = PartForm(instance=my_part)

        if my_part.type_parti !='gen':
            ctx['form_page'] = PageForm(instance=my_part.page)

        return ctx


    def post(self, request, *args, **kwargs):

        if 'rollback' in self.request.POST:

            prev_url = self.request.session['prev_url']

            return HttpResponseRedirect(prev_url)

        else:

            my_part = Parts.objects.get(id=kwargs['pk'])
            form_com = PartForm(request.POST,instance=my_part)

            if my_part.type_parti !='gen':

                form_page = PageForm(request.POST,instance=my_part.page)

                if form_page.is_valid() and form_com.is_valid():

                    obj_part = form_com.save()
                    #on va faire le lien page partie
                    form_page.instance.titre = "Page - "+form_com.instance.titre
                    obj_page = form_page.save()
                    obj_part.page = obj_page
                    obj_part.save()

                    return HttpResponseRedirect(self.request.session['prev_url'])

                else:

                    return render(request, self.template_name, {'form_part': form_com, 'form_page': form_page}, )
            else:

                if form_com.is_valid():

                    form_com.save()
                    return HttpResponseRedirect(self.request.session['prev_url'])

                else:

                    return render(request, self.template_name, {'form_part': form_com})


"""

Permission that permit 

"""
class CustomPermissionModule(UserPassesTestMixin):

    def retrieve_module_filiere(self,code):

        req = requests.get(settings.ELP_ETAPE+str(code)+"/")
        res = req.json()
        l_fil = []

        for etp in res:

            req2 = requests.get(settings.FILIERE_ETAPE + str(etp['code']) + "/"+str(etp['version']))
            res2 = req2.json()

            l_fil.append(str(res2[0]['id']))

        return l_fil

    def test_filiere(self,user_fil,mod_fil):

        for fil in user_fil:

            if fil.code in mod_fil:

                return True

        return False

    def module_in_filiere(self):

        res = False

        #retrieve user filiere
        try:

            user = self.request.user
            fil = Filiere.objects.filter(utilisateur_filiere=user)

        except Exception:

            return False #user doesnt have a filiere

        #retrieve module filiere
        module_filiere = self.retrieve_module_filiere(self.kwargs['code'])

        res = self.test_filiere(fil,module_filiere)

        return res

    def test_func(self):


        if self.request.user.is_superuser:

            test = True

        else:
            test = self.module_in_filiere()
            print('---test')
            print(test)
        return test

    #définie ce sue l'utilisateur verra si il n'est pas authorise
    def handle_no_permission(self):

        return HttpResponse('Vous n\'êtes pas autorisé à modifier cet élément')

class CustomCalendarPermission(CustomPermissionModule):

    def test_func(self):

        first = super(CustomCalendarPermission,self).test_func()

        if self.request.user.is_superuser:

            return True
        else:

            return first and self.request.user.has_perm('charon_db.edit_calendar')

"""

View that permit to edit a Module Content

"""
class EditModuleView(CustomPermissionModule,UpdateView):

    model = Module
    form_class = ModuleFormUpdated
    success_url = '/db/list_mod/'

    # fields = ['titre','periode','responsable','credits','description','competence','bibliographie','date_debut_examen','date_fin_examen','salle_examen']
    # fields = '__all__'

    def convert_periode(self,periode):


        if periode == '-1' or len(periode)>1:

            return '0'

        else:

            return periode

    def get_initial(self):

        initial = super(EditModuleView,self).get_initial()

        initial['user'] = self.request.user

        return initial

    def construct_list_inter(self,l_inter):

        res =''

        for inter in l_inter:

            res+=inter['prenom'].capitalize() + " " +inter['nom'].upper()+", "

        return res[:-2]

    def convert_credit(self,val):

        if val:

            return str(int(float(val)))

        else:


            return 'Aucun'

    def maquette_retrieve_max(self,code,module):

        try:

            req = requests.get(url=settings.MODULE+str(code)+"/")
            res = req.json()

            try:
                an = Annee.objects.get(current=True)
                req2 = requests.get(url=settings.INTERVENANTS+str(code)+"/"+str(an.cod_anu))
                res2 = req2.json()
                print('inter')
                print(res2)

                inter = self.construct_list_inter(res2)

                req3 = requests.get(url=settings.RESPONSABLES+str(code)+"/"+str(an.cod_anu))
                res3 = req3.json()

                print('resp')
                print(res3)

                resp = self.construct_list_inter(res3)

            except Exception as e:
                print(e)
                resp = ''
                inter = ''


            module.titre = str(res['lib'])
            module.periode = self.convert_periode(str(res['periode']))
            module.responsable = resp
            module.intervenant = inter
            module.credits = self.convert_credit(res['_ects'])
            module.description = str(res['descriptif'])
            module.objectif = str(res['objectifs'])
            module.bibliographie = str(res['biblio'])


        except Exception as e:
            print(e)
            pass


        return module

    def get_object(self, queryset=None):

        module,created =  Module.objects.get_or_create(code=self.kwargs['code'],annee_module=Annee.objects.get(current=True))
        self.request.session['prev_url'] = '/db/edit_module/'+str(self.kwargs['code'])

        if created:

            module = self.maquette_retrieve_max(self.kwargs['code'],module)
            print(module.__dict__)
            module.save()

        return module

    # def get_initial(self):
    #
    #     try:
    #
    #         Module.objects.get(code=self.kwargs['code'],annee_module=Annee.objects.get(current=True))
    #
    #         return super(EditModuleView,self).get_initial()
    #
    #     except Module.DoesNotExist:
    #
    #         module = Module(code=self.kwargs['code'],annee_module=Annee.objects.get(current=True))
    #         module = self.maquette_retrieve_max(self.kwargs['code'], module)
    #
    #         return {'code':module.code,
    #                 'titre':module.titre,
    #                 'periode':module.periode,
    #                 'responsable':module.responsable,
    #                 'intervenant':module.intervenant,
    #                 'credits':module.credits,
    #                 'description':module.description,
    #                 'competence':module.competence,
    #                 'bibliographie':module.bibliographie,
    #                 'date_debut_examen':module.date_debut_examen,
    #                 'date_fin_examen': module.date_debut_examen,
    #                 'date_debut_examen_r': module.date_debut_examen,
    #                 'date_fin_examen_r': module.date_debut_examen,
    #                 'salle_examen':module.salle_examen,
    #                 'salle_examen_r':module.salle_examen_r,
    #                 'examen_periode_1':module.examen_periode1,
    #                 'examen_periode_2':module.examen_periode2,
    #                 'objectif':module.objectif,
    #                 'prerequis':module.prerequis,
    #                 'annee_module':module.annee_module}

"""

Filter for specific part to select a guide

"""
class GuideFilter(django_filters.FilterSet):

    def guide_returning(request):

        return Guide.objects.filter(annee_guide=Annee.objects.get(current=True))

    def part_returning(queryset,field,value):

        qs = []

        return queryset.filter(guide=value)

    liste_guide = django_filters.ModelChoiceFilter(queryset=guide_returning,method=part_returning,label='Choisir un guide ')

    class Meta:

        model = Parts
        fields = {'liste_guide':['exact'] }



"""

View that permit to list specific part (guide related)

"""

class ListSpecificPart(FilterView):

    filterset_class = GuideFilter

    def get_queryset(self):

        self.request.session['prev_url'] = '/db/list_part_gui/'

        return Parts.objects.filter(type_parti='gui',annee_guide=Annee.objects.get(current=True)).order_by('titre')



class DeleteCalendarGeneralView(IsSuperUserPermissionMixin,DeleteView):

    model = GeneralCalendar
    success_url = '/db/general_cal/'
    template_name = 'charon_db/delete_general_event.html'

class DeleteToken(IsSuperUserPermissionMixin,DeleteView):

    model = KeyValue
    success_url = '/db/list_token/'
    template_name = 'charon_db/delete_token.html'


"""

View That permit to delete a part

"""

class DeletePart(IsSuperUserPermissionMixin,DeleteView):

    model = Parts
    success_url = '/db/list_part'

    def post(self, request, *args, **kwargs):

        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(DeletePart, self).post(request, *args, **kwargs)

class DeletePartCom(DeletePart):

    success_url = '/db/list_part_com'

"""

View That permit to delete a guide

"""

class DeleteGuide(IsSuperUserPermissionMixin,DeleteView):

    model = Guide
    success_url = '/db/list_guide'

    def post(self, request, *args, **kwargs):

        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(DeleteGuide, self).post(request, *args, **kwargs)


"""

View that list all the guide available + show historique

"""
class ListHistoriqueGuide(ListView):

    pass

"""

View That permit to generate a guide in the list or dowload a preview

"""
class GenerateGuide(ListView):

    pass

"""

View that permit to edit module according to Apogee Structure

"""
class EditStructureModule(TemplateView):

    pass


"""

View that generate a list of "partie commune"


"""
class ListGeneralPart(IsSuperUserPermissionMixin,ListView):

    def get_queryset(self):

        return Parts.objects.filter(type_parti='com',annee_guide=Annee.objects.get(current=True)).order_by('titre')


"""

View that permit to add cover for a guide

"""
class ListCouverture(IsSuperUserPermissionMixin,ListView):

    def get_queryset(self):

        return Guide.objects.filter(annee_guide=Annee.objects.get(current=True)).order_by('intitule')

"""

Class that permit to change the couverture file

"""
class CouvertureUpdate(IsSuperUserPermissionMixin,UpdateView):

    model = Guide
    fields = ['couverture']
    template_name = 'charon_db/edit_guide_couverture_form.html'
    success_url = '/db/list_couverture/'


"""

View that permit to edit all general date

"""
class ListCalendar(IsSuperUserPermissionMixin,TemplateView):

    template_name = 'add_general_event.html'

    def get_context_data(self, **kwargs):

        ctx = super(ListCalendar,self).get_context_data(**kwargs)
        ctx['events'] = GeneralCalendar.objects.filter(annee=Annee.objects.get(current=True)).order_by('debut')

        return ctx

class ModalCalendarAddEvent(BSModalCreateView):

    template_name = 'create_calendargeneral.html'
    form_class = GeneralCalendarModalForm
    success_message = 'Evènement ajouter avec succès'
    success_url = '/db/general_cal'

class ModalCalendarUpdateEvent(BSModalUpdateView):

    template_name = 'update_event.html'
    form_class = GeneralCalendarModalForm
    success_message = 'Evènement ajouter avec succès'
    success_url = '/db/general_cal'

    def get_queryset(self):

        id = self.kwargs['pk']

        return GeneralCalendar.objects.filter(id=id)

class ModalToken(IsSuperUserPermissionMixin,BSModalCreateView):

    template_name = 'create_token.html'
    form_class = ModalTokenForm
    success_message = 'Token ajouter avec succès'
    success_url = '/db/list_token'

class ModalUpdateToken(IsSuperUserPermissionMixin,BSModalUpdateView):

    template_name = 'update_token.html'
    form_class = ModalTokenForm
    success_message = 'Token modifié avec succès'
    success_url = '/db/list_token'

    def get_queryset(self):

        id = self.kwargs['pk']

        return KeyValue.objects.filter(id=id)


"""

"""


"""

View that permit to edit module calendar according to Apogee Structure

"""
class EditCalendarStructureModule(TemplateView):

    template_name = 'modules_calendar.html'


class ModuleCalendarUpdateDate(CustomCalendarPermission,BSModalUpdateView):

    template_name = 'create_calendargeneral.html'
    form_class = ModuleCalendarUpdateDateForm
    success_message = 'Date d\'examen pris en compte'
    success_url = '/db/general_cal'
    model = Module

    def get_object(self, queryset=None):

        module,created =  Module.objects.get_or_create(code=self.kwargs['code'],annee_module=Annee.objects.get(current=True))

        return module



"""

View that list all Etape managed by the CTU

"""
class ListModStructView(TemplateView):

    template_name = 'modules.html'


"""

View that list all module of an Etape and permit Edit

"""

class StructureModuleView(TemplateView):

    pass


"""

View that permit to test each pdfkit generation

"""
class GuideGeneratorView(IsSuperUserPermissionMixin,View):


    def get(self,*args,**kwargs):

        #test with prefixed guide
        id = kwargs['pk']
        guide = Guide.objects.get(id=id)
        guide.last_download = datetime.datetime.now()
        guide.save()
        gg = GuideGeneratorTex(guide)
        file = gg.build()


        historique = Historique(derniere_publication=datetime.datetime.now())
        historique.fichier.save(str(guide.id)+"-"+str(datetime.datetime.now())+'.pdf',File(file),True)
        guide.historique.add(historique)

        response = HttpResponse(file.getvalue(), content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename="' + quote(guide.intitule) + '.pdf"'

        return response

"""

View that permit to test each pdfkit generation

"""
class GuideGeneratorTestView(View):


    def get(self,*args,**kwargs):

        #test with prefixed guide
        id = kwargs['pk']
        guide = Guide.objects.get(id=id)
        guide.last_download = datetime.datetime.now()
        guide.save()
        gg = GuideGeneratorTex(guide)
        file = gg.build(brouillon=True)

        response = HttpResponse(open(file, 'rb'), content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename="' + quote(guide.intitule) + '.pdf"'

        return response


"""

View that permit to make a copy an create new year

"""

"""

View that permit to test each pdfkit generation

"""
class YearCopy(IsSuperUserPermissionMixin,TemplateView):

    template_name = 'year_copy.html'

    def iso_date(self,date,pas,copy_time):

        if date:

            a,w,d = datetime.date(date.year,date.month,date.day).isocalendar()
            n_date = iso_to_gregorian(int(a+pas),w,d)

            if copy_time:

                return datetime.datetime(n_date.year,n_date.month,n_date.day,hour=date.hour,minute=date.minute,second=date.second,microsecond=date.microsecond)

            else:

                return n_date
        else:

            return None

    def copy_general_calendar(self,nouvelle,current):

        for event in GeneralCalendar.objects.filter(annee=current):

            event.pk = None
            event.id=None
            event.annee = nouvelle
            pas = int(nouvelle.cod_anu) - int(current.cod_anu)
            event.debut = self.iso_date(event.debut,pas,copy_time=False)
            event.fin = self.iso_date(event.fin,pas,copy_time=False)
            event.save(copy=True)

    def copy_module(self,nouvelle,current):

        for mod in Module.objects.filter(annee_module=current):

            mod.pk = None
            mod.id = None #we delete pk and id from object to copy
            mod.annee_module = nouvelle #we give next year

            pas = int(nouvelle.cod_anu) - int(current.cod_anu)
            mod.date_fin_examen_r = self.iso_date(mod.date_fin_examen_r,pas,copy_time=True)
            mod.date_fin_examen = self.iso_date(mod.date_fin_examen,pas,copy_time=True)
            mod.date_debut_examen_r = self.iso_date(mod.date_debut_examen_r,pas,copy_time=True)
            mod.date_debut_examen = self.iso_date(mod.date_debut_examen,pas,copy_time=True)

            mod.save(copy=True) #and save

    def copy_guide(self,year,current):

        mem_part_com = {}

        for guide in Guide.objects.filter(annee_guide=current):

            id_guide = guide.id
            n_guide = guide
            parties = guide.parti.all()
            n_guide.pk = None
            n_guide.id = None
            n_guide.annee_guide = year
            n_guide.save()

            for part in parties:

                part_id = part.id
                page = part.page

                if (part.type_parti == 'com' and str(part_id) not in mem_part_com) or part.type_parti != 'com': # si la parti n'existe pas et est commune on la recréer et on refait sa page

                    n_part = part
                    n_part.pk = None
                    n_part.id = None
                    n_part.annee_guide = year
                    n_part.save() #we copy part

                    #on oublie pas d'ajouter la page commune pour mémoire
                    if part.type_parti =='com':

                        mem_part_com.update({str(part_id):str(n_part.id)})

                    # then page
                    if page:
                        print('-- oh une page')
                        print(page)
                        page.pk = None
                        page.id = None
                        page.save()
                        n_part.page = page
                        n_part.save()

                else:
                    #sinon si elle existe et quelle est de type commune on la recupère
                    if part.type_parti =='com':

                        new_id = mem_part_com[str(part.id)] #on recupere le nouvelle id
                        n_part = Parts.objects.get(id=new_id)

                #after order
                try:

                    opg = OrdrePartiGuide.objects.get(guide_id=id_guide, partie_id=part_id)
                    n_opg = OrdrePartiGuide(guide=n_guide,partie=n_part,numero_ordre=opg.numero_ordre)
                    n_opg.save()


                except Exception as e:

                    print(e)


                #finally we link to the guide
                print('-- une n part ajout au guide')
                n_guide.parti.add(n_part) #we add copy of part to copy of guide

    def copy_tags(self,year,current):

        print(KeyValue.objects.filter(annee_guide_id=1))

        for tags in KeyValue.objects.filter(annee_guide=1):

            n_tags = tags
            n_tags.pk = None
            n_tags.id = None #we delete pk and id from object to copy
            n_tags.annee_guide = year #we give next year
            n_tags.save(copy=True)

    def launch_copy(self,year):

        current = Annee.objects.get(current=True)

        self.copy_module(year,current)
        self.copy_guide(year,current)
        self.copy_general_calendar(year,current)
        self.copy_tags(year,current)

    def get_context_data(self, **kwargs):

        ctx = super(YearCopy,self).get_context_data(**kwargs)
        ctx['current'] = Annee.objects.get(current=True)
        ctx['years'] = Annee.objects.all().order_by('cod_anu')

        return ctx

    def create_year(self):

        court = self.request.POST.get('court')
        long = self.request.POST.get('long')
        an = Annee(cod_anu=court,nom_long=long,current=False)

        an.save()

        return an

    def set_current(self,year):

        Annee.objects.all().update(current=False)

        if year:

            year.current = True
            year.save()

        else:

            year = self.request.POST.get('year')
            an = Annee.objects.get(id=year)
            an.current = True
            an.save()

    @transaction.atomic #with atomic transaction no worries in case the copy goes wrong its not saved
    def post(self, request, *args, **kwargs):

        year = None

        if 'copy' in self.request.POST:

            # first we create new year
            year = self.create_year()

            # then we launch a copy with this new year
            self.launch_copy(year)

            # finaly we set current if asked

        if 'change' in self.request.POST or self.request.POST.get('current'):

            self.set_current(year)

        return HttpResponseRedirect('/db/copy')


class Home(TemplateView):

    template_name = 'home.html'


class ListTokenList(TemplateView):

    template_name = 'list_key.html'

    def get_context_data(self, **kwargs):

        ctx = super(ListTokenList,self).get_context_data(**kwargs)
        ctx['tokens'] = KeyValue.objects.filter(annee_guide=Annee.objects.get(current=True)).order_by('titre')

        return ctx

class MinimalMod(TemplateView):

    template_name = 'update_minimum.html'

    def convert_periode(self,periode):


        if periode == '-1' or len(periode)>1:

            return '0'

        else:

            return periode

    def convert_credit(self,val):

        if val:

            return str(int(float(val)))

        else:


            return 'Aucun'

    def construct_list_inter(self,l_inter):

        res =''

        for inter in l_inter:

            res+=inter['prenom'].capitalize() + " " +inter['nom'].upper()+", "

        return res[:-2]

    def maquette_retrieve_max(self,code):

        try:

            req = requests.get(url=settings.MODULE+str(code)+"/")
            res = req.json()

            try:
                an = Annee.objects.get(current=True)
                req2 = requests.get(url=settings.INTERVENANTS+str(code)+"/"+str(an.cod_anu))
                res2 = req2.json()

                inter = self.construct_list_inter(res2)

                req3 = requests.get(url=settings.RESPONSABLES+str(code)+"/"+str(an.cod_anu))
                res3 = req3.json()

                resp = self.construct_list_inter(res3)

            except Exception as e:

                print(e)
                resp = ''
                inter = ''

            module = Module.objects.get(code=code,annee_module=Annee.objects.get(current=True))
            module.titre = str(res['lib'])
            module.periode = self.convert_periode(str(res['periode']))
            module.responsable = resp
            module.intervenant = inter
            module.credits = self.convert_credit(res['_ects'])
            module.save()

        except Exception as e:

            pass

    def post(self, request, *args, **kwargs):

        if 'valider' in self.request.POST:

            self.maquette_retrieve_max(self.kwargs['code'])

            return HttpResponseRedirect(self.request.session['prev_url'])

        else:

            return HttpResponseRedirect(self.request.session['prev_url'])
