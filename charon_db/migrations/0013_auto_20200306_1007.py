# Generated by Django 2.0 on 2020-03-06 10:07

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('charon_db', '0012_auto_20200306_1006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='competence',
            field=ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Compètence du module'),
        ),
    ]
