# Generated by Django 2.0 on 2020-04-03 09:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charon_db', '0029_auto_20200403_0744'),
    ]

    operations = [
        migrations.AddField(
            model_name='parts',
            name='key',
            field=models.CharField(default='', max_length=150, verbose_name='Référnece texte'),
        ),
    ]
