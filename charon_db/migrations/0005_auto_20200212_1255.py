# Generated by Django 2.0 on 2020-02-12 12:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charon_db', '0004_auto_20200211_0940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guide',
            name='derniere_modification',
            field=models.DateTimeField(auto_now=True, verbose_name='Dernière Modification'),
        ),
        migrations.AlterField(
            model_name='parts',
            name='sous_type',
            field=models.CharField(choices=[('Couverture', 'cou'), ('Contact Filière', 'gui'), ('Calendrier Général', 'cal'), ('Calendrier Examens', 'exa'), ('MCC', 'mcc'), ('Structure Année', 'str'), ('Description des modules', 'mod')], max_length=3),
        ),
    ]
