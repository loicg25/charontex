# Generated by Django 2.0 on 2020-04-14 09:00

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charon_db', '0033_auto_20200410_1026'),
    ]

    operations = [
        migrations.AddField(
            model_name='generalcalendar',
            name='colour',
            field=models.CharField(blank=True, default='', max_length=50, verbose_name='Couleur'),
        ),
        migrations.AddField(
            model_name='generalcalendar',
            name='is_bold',
            field=models.BooleanField(default=False, verbose_name='Afficher en gras'),
        ),
        migrations.AddField(
            model_name='generalcalendar',
            name='is_title',
            field=models.BooleanField(default=False, verbose_name='Est un titre de partie'),
        ),
        migrations.AlterField(
            model_name='module',
            name='objectif',
            field=ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Objectifs pédagogique'),
        ),
    ]
