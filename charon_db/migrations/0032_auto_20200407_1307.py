# Generated by Django 2.0 on 2020-04-07 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charon_db', '0031_auto_20200406_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='credits',
            field=models.CharField(blank=True, default='', max_length=30, verbose_name='Crédit'),
        ),
    ]
