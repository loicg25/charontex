# Generated by Django 2.0 on 2020-03-09 15:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('charon_db', '0013_auto_20200306_1007'),
    ]

    operations = [
        migrations.AddField(
            model_name='generalcalendar',
            name='annee',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='charon_db.Annee'),
        ),
    ]
