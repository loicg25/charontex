from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, make_aware
import csv

from charon_db.models import Module,Annee

"""

Command that permit to inject module date into charon

"""
class Command(BaseCommand):

    def str_to_aware_dt(self,date_str):

        res = parse_datetime(date_str)

        if not is_aware(res):

            res = make_aware(res)

        return res

    def add_arguments(self, parser):

        parser.add_argument(
            '--create',
            action='store_true',
            help='Creer le module si il n\'existe pas. Attention peu être source de conflit',
        )

    def handle(self, *args, **options):

        with open('dates.csv') as file:

            reader = csv.reader(file,delimiter=';')

            for row in reader:

                try:

                    if options['create']:

                        module,created = Module.objects.get_or_create(code=row[0],annee_module=Annee.objects.get(current=True))

                    else:

                        #0:code 1:periode1 2:periode2 3:debut 4:fin 5:salle 6:debut_r 7:fin_r 8:salle_r
                        module = Module.objects.get(code=row[0],annee_module=Annee.objects.get(current=True))

                    if row[1] == 'x':
                        module.examen_periode1 = True
                    else:
                        module.examen_periode1 = False

                    if row[2] == 'x':
                        module.examen_periode2 = True
                    else:
                        module.examen_periode2 = False

                    try:
                        if module.date_debut_examen:
                            module.date_debut_examen = self.str_to_aware_dt(row[3]+" "+row[4])
                        else:
                            module.date_debut_examen = None

                        if module.date_fin_examen:
                            module.date_fin_examen = self.str_to_aware_dt(row[5]+" "+row[6])
                        else:
                            module.date_fin_examen = None

                        if module.salle_examen:
                            module.salle_examen = row[7]
                        else:

                            module.salle_examen

                    except Exception:
                        module.date_debut_examen = None
                        module.date_fin_examen = None
                        module.salle_examen = ''
                        print('pas de date examen session 1')

                    try:

                        if module.date_debut_examen_r:
                            module.date_debut_examen_r = self.str_to_aware_dt(row[8]+" "+row[9])
                        else:

                            module.date_debut_examen_r = None

                        if module.date_fin_examen_r:

                            module.date_fin_examen_r = self.str_to_aware_dt(row[10]+" "+row[11])

                        else:

                            module.date_fin_examen_r = None

                        try:

                            if module.salle_examen_r:

                                module.salle_examen_r = row[12]

                            else:

                                module.salle_examen_r = ''

                        except Exception:

                            module.salle_examen_r = ''

                    except Exception:

                        module.date_debut_examen_r = None
                        module.date_fin_examen_r = None
                        module.salle_examen_r = ''
                        print('pas de date examen session 2')

                    module.save()
                    self.stdout.write(self.style.SUCCESS('Import avec succès du code : ' + str(row[0])))

                except Exception as e:

                    self.stdout.write(self.style.ERROR('Echec d\'import du code : '+str(row[0])))
