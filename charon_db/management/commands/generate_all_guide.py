from django.core.management.base import BaseCommand
from charon_db.models import *
from charon_lib.libs import *
from pathlib import Path
from datetime import datetime

class Command(BaseCommand):

    def handle(self, *args, **options):

        Path("/code/guide_generated").mkdir(parents=True, exist_ok=True)

        guides = Guide.objects.filter(annee_guide=Annee.objects.get(current=True))

        for guide in guides:

            gg = GuideGenerator(guide)
            file_content = gg.build()
            date = datetime.now().strftime("%m%d%Y_%H%M%S")
            f = open('/code/guide_generated/'+str(guide.intitule)+'_'+str(date)+".pdf",'wb')
            f.write(file_content.getvalue())
            f.close()


