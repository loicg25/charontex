from django.apps import AppConfig


class CharonDbConfig(AppConfig):
    name = 'charon_db'
