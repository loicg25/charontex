from django import template
from charon_db.models import Module,Annee

register = template.Library()

def periode_str(periode):
    if periode !=0:
        return str(periode)
    else:
        return 'A'

def value_or_nc(val):
    if val:
        return str(val)
    else:
        return 'N/C'

def periode_color(periode):
    if periode == 1:
        return 'OrangeRed'
    elif periode == 2:
        return 'SteelBlue'
    else:
        return 'SeaGreen'

def child_html(childs,tabi="\;\;\;"):
    html = ""
    for child in sorted(childs,key = lambda mod : mod['num_pre_elp']):
        #il a des enfants il n'a donc pas de periode
        if child['children']:
            html += '\cellcolor{LightGrey} ~ & '+tabi+ "\\textbf{\hyperref["+child['anchor']+"-"+child['code']+"]{" +child['code']+" - "+child['lib'] + "}} & \cellcolor{DarkGrey} \\textbf{" + value_or_nc(child['ects']) + "} \\\\" \
            + child_html(child['children'], tabi+"\;\;" )
        else:
            html += "\cellcolor{" + periode_color(child['periode']) + "} \\textcolor{white}{\\textbf{ " + periode_str(child['periode']) + " → }} & " +tabi+ "\hyperref["+child['anchor']+"-"+child['code']+"]{" +child['code']+" - "+child['lib'] + "} & \cellcolor{DarkGrey} " + value_or_nc(child['ects']) + "\\\\"
    return html


@register.filter(name='latex_tree')
def latex_tree(tree):
    html ='\\begin{tabular}{ C{2cm} p{11.1cm} C{2cm}}'
    for elt in sorted(tree,key = lambda mod : mod['num_pre_elp']):
        #pas de periode il est puni
        if elt['children']:
            html+= "\cellcolor{LightGrey}~ & \\textbf{\hyperref["+elt['anchor']+"-"+elt['code']+"]{" +elt['code']+" - "+elt['lib'] + "}} & \cellcolor{DarkGrey} \\textbf{" +value_or_nc(elt['ects'])+"} \\\\" \
                +child_html(elt['children'])
        #on indique sa periode
        else:
            html+= "\cellcolor{"+periode_color(elt['periode'])+"} \\textcolor{white}{ \\textbf{"+periode_str(elt['periode'])+" → }} & \\textbf{\hyperref["+elt['anchor']+'-'+elt['code']+"]{" +elt['code']+" - "+elt['lib'] + "}} & \cellcolor{DarkGrey} \\textbf{" + value_or_nc(elt['ects']) +"}\\\\"
    html+="\end{tabular}"
    return html








