from django import template
from django.utils.html import mark_safe
import html

from charon_libtex.lib.parserHTML import MyHTMLParser

register = template.Library()


def protectAndUnescapeText(text, json):
    ut = html.unescape(text)
    protectEsper = ut.replace("&", "\&")
    protectPourc = protectEsper.replace("%", "\%")
    protectDie = protectPourc.replace("#", "\#")
    protectUnde = protectDie.replace("_", "\_")
    if not json:
        protectAcOuv = protectUnde.replace("{", "\{")
        protectAcFerm = protectAcOuv.replace("}", "\}")
        return protectAcFerm
    else:
        return protectUnde


def prepare_text(text):
    parser = MyHTMLParser()
    parser.feed(text)
    return parser.getOutput()

def text_semestre(semestre):
    if semestre == "1":
        return '1er Semestre'
    else:
        return '2ème Semestre'

def text_periode(periode):
    if periode == 1:
        return 'Enseigné en période 1 (octobre à janvier)'
    elif periode == 2:
        return 'Enseigné en période 2 (février à mai)'
    else:
        return 'Enseigné toute l\'année'

def empty_or_biblio(val):
        res = ""
        if val:
            res ="\indent {\\itshape \\underline{BIBLIOGRAPHIE INDICATIVE :} \\\\ \n\n "  + empty_or_val(val) + "} ~\\\\  "
            return res
        else:
            return res

def empty_or_val(val):
    r_val = val
    if r_val:
        return val
    return ' '

def child_html(childs,semestre):
    html = ""
    for elt in sorted(childs,key = lambda mod : mod['num_pre_elp']):
        if elt['children']:
            html += "\\textbf{\large →" + str(elt['lib']) + "} \\\\[0.2cm]\n\n" \
            "\\textbf{"+str(elt['code'])+"}\phantomsection\label{"+str(elt['key'])+"-"+str(elt['code'])+"} | "+str(elt['ects'])+" ECTS "+"}\\\\[0.1cm]\n\n " \
            "\\textbf{Enseignant : "+ str(elt['module'].intervenant)+"}\\\\\n\n" \
            + small_part_or_not(prepare_text(elt['module'].description), 'Description') + " " \
            + small_part_or_not(prepare_text(elt['module'].prerequis), 'Prerequis') + " " \
            + small_part_or_not(prepare_text(elt['module'].objectif), 'Objectifs') + " " \
            + small_part_or_not(prepare_text(elt['module'].competence), 'Compétences à acquérir') + " " \
            + small_part_or_not(prepare_text(elt['module'].bibliographie),'Bibliographie',True) + " " \
            + big_part_or_not('', 'Est composé de ',passe=True) + "\n\\begin{adjustwidth}{50pt}{0pt}\n " + child_html(elt['children'], semestre) + " \end{adjustwidth}\n\n\\newpage "
        else:
            html += "\\textbf{\large →"+ str(elt['lib']) + "} \\\\[0.2cm]\n\n"\
            "\\textbf{"+str(elt['code'])+"\phantomsection\label{"+str(elt['key'])+"-"+str(elt['code'])+"} | "+str(elt['ects'])+" ECTS | Période : "+str(elt['periode'])+"}\\\\[0.1cm]\n\n" \
            "\\textbf{Enseignant : "+str(elt['module'].intervenant)+"}\\\\\n\n" \
            + small_part_or_not(prepare_text(elt['module'].description),'Description')+" " \
            + small_part_or_not(prepare_text(elt['module'].prerequis),'Prerequis')+" " \
            + small_part_or_not(prepare_text(elt['module'].objectif),'Objectifs')+" " \
            + small_part_or_not(prepare_text(elt['module'].competence),'Compétences à acquérir')+" " \
            + small_part_or_not(prepare_text(elt['module'].bibliographie),'Bibliographie',True)
    return html

def responsable_or_not(responsable):
    if responsable and responsable != '':
        return "Responsable : \\textbf{"+str(responsable)+"}"
    else:
        return ""

def inter_or_not(intervenant):
    if intervenant and intervenant != '':
        return "Intervenant : \\textbf{"+str(intervenant)+"}"
    else:
        return ""

def small_inter_or_not(intervenant):
    if intervenant and intervenant != '':
        return "\\textbf{Enseignant : " + str(intervenant) + "" + "}"
    else:
        return ""

def small_part_or_not(val,title,italic=False,passe=False):
    if (val.strip() and val.strip() != '') or passe:
        part = ""
        if italic:
            part += "{\\itshape "
        part += "\indent \\textbf{\\uppercase{ "+title+"}} :\\\\\n\n\\begin{adjustwidth}{25pt}{0pt} "+str(val) +"\\end{adjustwidth} ~\\\\"
        if italic:
            part += "}"
        return part
    else:
        return ""

def big_part_or_not(val,title,passe=False):
    if len(val.strip()) > 0 or passe:
        part = "\indent \\textbf{\\uppercase{"+title+"}} :\\\\\n\n"
        if not passe:
            part += "\\begin{adjustwidth}{25pt}{0pt} "+str(val) +"\\end{adjustwidth} ~\\\\"
        return part
    else:
        return ""

@register.filter(name='latex_tree_module')
def latex_tree(tree,semestre):

    html = ''
    for elt in sorted(tree,key = lambda mod : mod['num_pre_elp']):
        if elt['children']:
            html+=  "\\noindent\sffamily\colorbox{black}{\\bfseries\large\\textcolor{white}{\,"+str(elt['code'])+"\phantomsection\label{"+str(elt['key'])+"-"+str(elt['code'])+"}\,}} " \
                "{\large \\bfseries "+str(elt['lib'])+"}\n\n{\\rule{12cm}{0.2mm} }\n\n" \
                "\sffamily\colorbox{black}{\\bfseries\large\\textcolor{white}{\:CREDITS : "+str(elt['ects'])+" ECTS\:}} \\\\" \
                " \\begin{itemize}[label=\\textbullet, font=\LARGE] \item"+text_semestre(semestre)+"\end{itemize}\\\\\n\n" \
                "\hdashrule{\linewidth}{0.5pt}{2pt}\n\n" \
                +responsable_or_not(prepare_text(elt['module'].responsable))+"\n\n" \
                "\hdashrule{\linewidth}{0.5pt}{2pt}\\\\\n\n" \
                "\\textbf{"+str(elt['code'])+" : "+str(elt['lib'])+"}\\\\\n\n" \
                +inter_or_not(prepare_text(elt['module'].intervenant))+"} \\\\\n\n" \
                +big_part_or_not(prepare_text(elt['module'].description),'Description')+""\
                +big_part_or_not(prepare_text(elt['module'].prerequis),'Prerequis')+" "\
                +big_part_or_not(prepare_text(elt['module'].objectif),'Objectifs')+" "\
                +big_part_or_not(prepare_text(elt['module'].competence),'Compétences à acquérir')+" "\
                +empty_or_biblio(prepare_text(elt['module'].bibliographie))+" "\
                +big_part_or_not('','Est composé de ',passe=True)+"\n\\begin{adjustwidth}{50pt}{0pt}\n"+child_html(elt['children'],semestre) + " \end{adjustwidth}\n\n\\newpage "
        else:
            html+=  "\\noindent\sffamily\colorbox{black}{\\bfseries\large\\textcolor{white}{\,"+str(elt['code'])+"\phantomsection\label{"+str(elt['key'])+"-"+str(elt['code'])+"}\,}} " \
                "{\large \\bfseries " + str(elt['lib']) + "}\n\n{\\rule{12cm}{0.2mm}}\n\n" \
                "\sffamily\colorbox{black}{\\bfseries\large\\textcolor{white}{\:CREDITS : "+str(elt['ects'])+" ECTS\:}}\\\\" \
                " \\begin{itemize}[label=\\textbullet, font=\LARGE]\item"+text_semestre(semestre)+" " \
                " \item "+text_periode(elt['periode'])+" \end{itemize}\\\\\n\n" \
                "\hdashrule{\linewidth}{0.5pt}{2pt}\n\n" \
                + responsable_or_not(prepare_text(elt['module'].responsable)) + "\n\n" \
                "\hdashrule{\linewidth}{0.5pt}{2pt}\\\\\n\n" \
                "\\textbf{" + str(elt['code']) + " : " + str(elt['lib']) + "}\\\\\n\n" \
                +inter_or_not(prepare_text(elt['module'].intervenant)) + "} \\\\\n\n" \
                +big_part_or_not(prepare_text(elt['module'].description),'Description')+" " \
                +big_part_or_not(prepare_text(elt['module'].prerequis),'Prerequis')+" " \
                +big_part_or_not(prepare_text(elt['module'].objectif),'Objectifs')+" " \
                +big_part_or_not(prepare_text(elt['module'].competence),'Compétences à acquérir')+" " \
                +empty_or_biblio(prepare_text(elt['module'].bibliographie)) + "\n\n\\newpage "
    return html