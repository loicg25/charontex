from django import template

register = template.Library()

@register.filter(name='c_word_wrap')
def word_wrap(val):

    plus = ''

    if len(val)>60:

        plus = ' ...'

    return val[:60]+plus