from django import template

register = template.Library()

@register.filter(name='delta')
def delta(mod):

    try:
        delta = mod.date_fin_examen - mod.date_debut_examen

        return delta.seconds // 3600

    except Exception:

        return ''


@register.filter(name='delta_r')
def delta_r(mod):
    try:
        delta = mod.date_fin_examen_r - mod.date_debut_examen_r

        return delta.seconds // 3600

    except Exception:

        return ''


@register.filter(name='last_line')
def is_last_line(current,iter):


    if iter[len(iter)-1] == current:

        return ''

    else:

        return 'border-bottom:1px dashed black;'