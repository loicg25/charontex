from django.forms import ModelForm,CharField
from django.forms import forms
from charon_db.models import Module,Parts,Page,GeneralCalendar,KeyValue
from bootstrap_modal_forms.forms import BSModalForm
from bootstrap_datepicker_plus import DateTimePickerInput
from django.forms.widgets import TextInput

class ModuleFormUpdated(ModelForm):

    class Meta:

        model = Module
        exclude = ['annee_module',]

    code = CharField(disabled=True)
    titre = CharField(disabled=True)
    periode = CharField(disabled=True)
    credits = CharField(disabled=True)

    def __init__(self,*args,**kwargs):

        user = kwargs['initial'].pop('user')
        super(ModuleFormUpdated,self).__init__(*args,**kwargs)

        if not user.is_superuser:

            self.fields['intervenant'].widget.attrs['readonly'] = True
            self.fields['responsable'].widget.attrs['readonly'] = True


class ModuleForm(ModelForm):

    class Meta:

        model = Module
        fields = '__all__'

class PartComForm(ModelForm):

    class Meta:

        model = Parts
        fields = ['titre','key']

class PartForm(ModelForm):

    def __init__(self,*args,**kwargs):

        super(PartForm,self).__init__(*args,**kwargs)
        # self.fields['type_parti'].widget.attrs['readonly'] = True


    class Meta:

        model = Parts
        exclude =  ['type_parti','sous_type','annee_guide']


class PageForm(ModelForm):

    class Meta:

        model = Page
        fields = ['contenu_page',]

class ModalTokenForm(BSModalForm):

    class Meta:

        model = KeyValue
        exclude = ['annee_guide']

"""

Modal form for adding Date for general calendar

"""
class GeneralCalendarModalForm(BSModalForm):

    class Meta:

        model = GeneralCalendar
        exclude = ['annee',]

    def __init__(self, *args, **kwargs):

        super(GeneralCalendarModalForm, self).__init__(*args, **kwargs)
        self.fields['colour'].widget = TextInput(attrs={'type':'color'})

        self.fields['debut'].input_formats = ['%d-%m-%Y']
        self.fields['fin'].input_formats = ['%d-%m-%Y']
        self.fields['debut'].widget = DateTimePickerInput(format='%d-%m-%Y',
                                                                            options={"format": "DD-MM-YYYY",
                                                                                     "locale":"fr",
                                                                                     "showClear": True,
                                                                                     "showClose": True})
        self.fields['fin'].widget = DateTimePickerInput(format='%d-%m-%Y à %H:%M',
                                                                          options={"format": "DD-MM-YYYY",
                                                                                   "locale": "fr",
                                                                                   "showClear": True,
                                                                                   "showClose": True})



class ModuleCalendarUpdateDateForm(BSModalForm):

    class Meta:

        model = Module
        fields = ['date_debut_examen','date_fin_examen','salle_examen','date_debut_examen_r','date_fin_examen_r','salle_examen_r','examen_periode1','examen_periode2']

    def __init__(self,*args,**kwargs):

        super(ModuleCalendarUpdateDateForm,self).__init__(*args,**kwargs)
        self.fields['date_debut_examen_r'].input_formats = ['%d-%m-%Y à %H:%M']
        self.fields['date_fin_examen_r'].input_formats = ['%d-%m-%Y à %H:%M']
        self.fields['date_debut_examen_r'].widget = DateTimePickerInput(format='%d-%m-%Y à %H:%M',
                                                                      options={"format": "DD-MM-YYYY à HH:mm",
                                                                               "locale": "fr",
                                                                               "showClear": True, "showClose": True})
        self.fields['date_fin_examen_r'].widget = DateTimePickerInput(format='%d-%m-%Y à %H:%M',
                                                                    options={"format": "DD-MM-YYYY à HH:mm",
                                                                             "locale": "fr",
                                                                             "showClear": True, "showClose": True})
        self.fields['date_debut_examen'].input_formats = ['%d-%m-%Y à %H:%M']
        self.fields['date_fin_examen'].input_formats = ['%d-%m-%Y à %H:%M']
        self.fields['date_debut_examen'].widget = DateTimePickerInput(format='%d-%m-%Y à %H:%M',options={"format":"DD-MM-YYYY à HH:mm","locale":"fr","showClear":True,"showClose":True})
        self.fields['date_fin_examen'].widget = DateTimePickerInput(format='%d-%m-%Y à %H:%M',options={"format":"DD-MM-YYYY à HH:mm","locale":"fr","showClear":True,"showClose":True})