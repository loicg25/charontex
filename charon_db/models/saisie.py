from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User


class Annee(models.Model):

    cod_anu = models.CharField(max_length=4,verbose_name=u"Code Année Apogée")
    nom_long = models.CharField(max_length=250,verbose_name="Nom long")
    current = models.BooleanField(default=True,verbose_name=u"Année Courante")

class Filiere(models.Model):

    intitule = models.CharField(verbose_name=u"Intitulé",max_length=250)
    code = models.CharField(verbose_name='Id maquette filiere',max_length=250,default='')
    utilisateur_filiere = models.ManyToManyField(User,verbose_name='Utilisateur')

    def __str__(self):


        return self.intitule

class Etape(models.Model):

    code = models.CharField(verbose_name=u"Code étape",max_length=250)
    version = models.CharField(verbose_name=u"Version d'étape",max_length=250)
    intitule = models.CharField(verbose_name=u"Version d'étape",max_length=250)
    filiere_appartenance = models.ForeignKey(Filiere,verbose_name="Filiere",on_delete=models.CASCADE)
    modules = models.ManyToManyField('Module',verbose_name="Modules")

class Contact(models.Model):

    fonction = models.CharField(verbose_name="Fonction",max_length=250)
    telephone = models.CharField(verbose_name=u"Téléphone",max_length=250)
    email = models.CharField(verbose_name="Adresse Mail",max_length=250)
    lien_etape = models.ManyToManyField('Etape',verbose_name="Etape lié")
    is_global = models.BooleanField(default=False)

class Module(models.Model):

    class Meta:

        permissions = (('edit_calendar','Peut modifier le calendrier d\'examen'),)

    code = models.CharField(verbose_name="Code",default="",max_length=150)
    titre = models.CharField(verbose_name="Titre",default="",max_length=150)
    periode = models.CharField(verbose_name="Periode",max_length=1,blank=True,null=True,default='')
    responsable = models.CharField(verbose_name="Responsable",max_length=250,blank=True,default='',null=True)
    intervenant = models.CharField(verbose_name="Intervenant",max_length=250,default='',blank=True,null=True)
    credits = models.CharField(verbose_name="Crédit",max_length=30,blank=True,default='')
    description = RichTextField(verbose_name="Description de module",blank=True,null=True,default='')
    prerequis = RichTextField(verbose_name="Prerequis du module",blank=True,null=True,default='')
    objectif = RichTextField(verbose_name="Objectifs pédagogique",blank=True,null=True,default='')
    competence = RichTextField(verbose_name="Compètence à acquérir du module",blank=True,null=True,default='')
    bibliographie = RichTextField(verbose_name="Bibliographie du module",blank=True,null=True,default='')
    date_debut_examen = models.DateTimeField(verbose_name="Date de Début examen",blank=True,null=True)
    date_fin_examen = models.DateTimeField(verbose_name="Date de Fin examen",blank=True,null=True)
    salle_examen = models.TextField(verbose_name="Salle d'examen",max_length=250,blank=True,default="")
    examen_periode1 = models.BooleanField(verbose_name="Examen Période 1",default=False)
    examen_periode2 = models.BooleanField(verbose_name="Examen Session 2",default=False)
    date_debut_examen_r = models.DateTimeField(verbose_name="Date de Début examen ratrapage", blank=True, null=True)
    date_fin_examen_r = models.DateTimeField(verbose_name="Date de Fin examen ratrapage", blank=True, null=True)
    salle_examen_r = models.TextField(verbose_name="Salle d'examen rattrapage",max_length=250,blank=True,default="")
    annee_module = models.ForeignKey(Annee,verbose_name="Annee Module",null=True,blank=True,on_delete=models.DO_NOTHING)

    def save(self, *args, **kwargs):

        if 'copy' in kwargs:

            copy = kwargs['copy']
            kwargs.pop('copy')

        else:

            copy = False


        if self.pk is None and not copy:  # newly created object so we can add new Annee if its update we dont touch year

            self.annee_module = Annee.objects.get(current=True)

        super(Module, self).save(*args, **kwargs)


    def __str__(self):

        return self.titre



class Guide(models.Model):

    intitule = models.CharField(verbose_name=u"Intitulé du Guide",max_length=150)
    prefixe = models.CharField(verbose_name=u"Suffixe fichier",max_length=100,default='')
    parti = models.ManyToManyField('Parts',verbose_name="Partie composant le guide")
    historique = models.ManyToManyField('Historique')
    derniere_modification = models.DateTimeField(verbose_name=u"Dernière Modification",auto_now=True)
    annee_guide = models.ForeignKey("Annee",verbose_name="Année du guide",on_delete=models.DO_NOTHING)
    couverture = models.FileField(verbose_name='Fichier de Couverture',default=None,null=True,blank=True)
    last_download = models.DateTimeField(null=True,default=None,blank=True,verbose_name="Dernière Génération")
    last_publish = models.DateTimeField(null=True,default=None,blank=True,verbose_name="Dernière Publication")

    def __str__(self):

        return self.intitule

class Historique(models.Model):

    derniere_publication = models.DateTimeField(verbose_name=u"Date de Publication")
    fichier = models.FileField(verbose_name=u"Fichier guide créer")

    class Meta:

        ordering = ['-derniere_publication']

class OrdrePartiGuide(models.Model):

    partie = models.ForeignKey("Parts",verbose_name="Parti du guide",on_delete=models.CASCADE)
    guide = models.ForeignKey("Guide",verbose_name="Guide concerné",on_delete=models.CASCADE)
    numero_ordre = models.IntegerField(verbose_name="Numéro d'ordre pour ce guide",default=0)

class Parts(models.Model):

    titre = models.CharField(verbose_name="Titre",default="",max_length=150)
    type_parti = models.CharField(choices=(('com','Commune'),('gui','Guide'),('gen','Généré')),max_length=3)
    sous_type = models.CharField(choices=(('cou','Couverture'),(u'cfi','Contact Filière'),(u'cal','Calendrier Général'),(u'exa','Calendrier Examens'),(u'mcc','MCC'),(u'str','Structure Année'),(u'mod','Description des modules')),max_length=3,blank=True,null=True,default=None)
    page = models.ForeignKey('Page',verbose_name="Page lié",blank=True,default=None,null=True,on_delete=models.CASCADE)
    parametre = models.CharField(verbose_name="Paramètres de partie",blank=True,null=True,default=None,max_length=250)
    annee_guide = models.ForeignKey(Annee,verbose_name="Annee guide",blank=True,null=True,on_delete=models.DO_NOTHING)
    key = models.CharField(max_length=150,verbose_name="Référence pour le texte",default='',blank=True)

    def __str__(self):

        return self.titre

    def get_sous_type(self):

        if self.sous_type:
            return self.sous_type
        else:

            return '*'

    def get_parametre(self):

        if self.parametre:
            return self.parametre
        else:

            return '*'

    def to_sortable_part(self):

        if self.type_parti == 'com':

            return 'item_sortable_blue'

        elif self.type_parti == 'gui':

            return 'item_sortable_green'

        elif self.type_parti == 'gen':

            return 'item_sortable_red'

class KeyValue(models.Model):

    titre = models.CharField(verbose_name="Titre",max_length=150)
    key = models.CharField(verbose_name=u"Clé utilisée (préfixé par ctu_ lors de l'utilisation",max_length=150)
    value = models.CharField(verbose_name="Valeur à substituer",max_length=150)
    annee_guide = models.ForeignKey("Annee",on_delete=models.DO_NOTHING)

    def save(self, *args, **kwargs):

        if 'copy' in kwargs:

            copy = kwargs['copy']
            kwargs.pop('copy')

        else:

            copy = False

        if self.pk is None and not copy:  # newly created object so we can add new Annee if its update we dont touch year
            self.annee_guide = Annee.objects.get(current=True)

        super(KeyValue, self).save(*args, **kwargs)

class Page(models.Model):

    titre = models.CharField(verbose_name="Titre de la partie",default="",max_length=150)
    contenu_page = RichTextField(verbose_name="Texte de la page")

    def __str__(self):

        return self.titre

class GeneralCalendar(models.Model):

    debut = models.DateField(verbose_name="Date Début")
    fin = models.DateField(verbose_name="Date Fin",blank=True,default=None,null=True)
    titre = models.CharField(verbose_name="Titre de l'événement",max_length=250)
    annee = models.ForeignKey('Annee',null=True,on_delete=models.DO_NOTHING)
    is_title = models.BooleanField(verbose_name='Est un titre de partie',default=False)
    is_bold = models.BooleanField(verbose_name='Afficher en gras',default=False)
    colour = models.CharField(verbose_name='Couleur',default='',blank=True,max_length=50)

    def __str__(self):

        return self.titre

    def save(self, *args, **kwargs):

        if 'copy' in kwargs:

            copy = kwargs['copy']
            kwargs.pop('copy')

        else:

            copy = False

        if self.pk is None and not copy:#newly created object so we can add new Annee if its update we dont touch year
            self.annee = Annee.objects.get(current=True)

        
        super(GeneralCalendar, self).save(*args,**kwargs)

