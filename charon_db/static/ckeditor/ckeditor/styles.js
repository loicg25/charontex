CKEDITOR.stylesSet.add( 'default',
[

    /* attention certain style ne s'applique qu'une fois l'élément séléctionner ils sont dit : contexte sensitive */
   { name : 'Ancre Neutre', element : 'a', styles : { 'text-decoration' : 'none','color':'black' } },
    {name: 'Typo Machine', element: 'tt'}
]);
