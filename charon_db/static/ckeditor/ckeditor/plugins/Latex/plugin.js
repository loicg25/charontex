CKEDITOR.plugins.add('Latex', {
    icons: 'latex',
    init: function (editor) {
        //Ajout de la fonctionnalité du plugin
        editor.addCommand('insertLatex', {
            exec: function (editor) {
                var selection = editor.getSelection();
                var text = selection.getSelectedText();
                if (text.length === 0) {
                    //si l'utilisateur souhaite cliquer puis entrer son code LaTeX
                    editor.insertHtml('<span class="latex" style="background: #ffe7e8; border: 2px solid #e66465;"> Entrer code LaTeX </span>', 'unfiltered_html');
                } else {
                    //si l'utilisateur souhaite entrer son code LaTeX puis cliquer sur le bouton du plugin
                    editor.insertHtml('<span class="latex" style="background: #ffe7e8; border: 2px solid #e66465;"> ' + text + '</span>', 'unfiltered_html');
                }
            }
        });
        //Ajout du bouton à l'éditeur
        editor.ui.addButton('Latex', {
            label: 'Insert Latex',
            command: 'insertLatex',
            toolbar: 'insert'
        });
    }
});