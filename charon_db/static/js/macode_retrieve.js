$(document).ready(function(){

    $('#id_code_matiere').autocomplete({

    source : function(typed,returned){

    var tmp_url = '/gh/proxy-matiere/'
    var url = tmp_url+$('#id_code').val()

    $.get(url,function (data){

        returned(data);
    })

    },

    select: function( event, ui ) {

        $('#id_code_matiere').val(ui.item.nMACODE)

        return false;
    }

    }).on('focus', function() { $(this).keydown(); }).autocomplete("instance")._renderItem = function(ul,item){

        return $("<li>").append("<div>"+item.sFICODEVET+'( '+item.sELCODE+' )'+"</div>").appendTo(ul)

    };

})