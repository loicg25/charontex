
$(document).ready(function(){

    $('#id_code').autocomplete({

    source : function(typed,returned){

    var tmp_url = '/gh/proxy-module/'
    var url = tmp_url+typed.term

    $.get(url,function (data){

        console.log(data)
        returned(data);
    })

    },
    minLength: 2,

    select: function( event, ui ) {

        $( "#id_code_matiere" ).val( ui.item.SMATIERE );


        return false;
    }

    }).autocomplete("instance")._renderItem = function(ul,item){

        return $("<li>").append("<div>"+item.sNOM_PATRO+'('+item.sNOM_USUEL+')'+" "+item.sPrenom+"</div>").appendTo(ul)

    };

})