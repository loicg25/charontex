
$(document).ready(function() {

    $('#filiere_select').select2();
    $('#diplome_select').select2();
    $('#etape_select').select2();
    filiere_retrieve();
    change_filiere_retrive();
    change_etape_retrieve();
    select_etape_etp();


});

function asc_sort(a,b){

    return ($(b).text()< $(a).text()) ? -1 : 1 // condition ternaire qui comprime le precedent sort

}

function complete_option(data,selector,identifier,text,no_empty=true){

    if (no_empty){

        $(selector).find('option').not(':first').remove();

    }

    for(var i=0;i<data.length;i++){

        var option = '<option value="'+data[i][identifier]+'">'+data[i][text]+'</option>'

        $(selector).append(option)

    }

    $(selector).sort(asc_sort).appendTo(selector)

    $(selector).trigger('change') //only do that at the end for opti

}

function compare_filiere(a,b){

    if (a['libelle'] < b['libelle']){

        return -1

    }else if (b['libelle'] > a['libelle']){

        return 1

    }else{

        return 0

    }

}

function filiere_retrieve(){

    $.get('/db/proxy_filiere',function(data){

        data.sort(compare_filiere)
        complete_option(data,'#filiere_select','id','libelle')

    })

}

function change_filiere_retrive(){

    $('body').on('change','#filiere_select',function () {

        $('#diplome_select').find('option').not(':first').remove();
        $('#etape_select').find('option').not(':first').remove();

        var id = $('#filiere_select').val()

        $.get('/db/proxy_filiere_info/?id='+id,function(data){
            console.log(data)
            var sorted_dip = data[0]['diplome_r_ref'].sort(compare_filiere)
            // complete_option(data[0]['diplome_r_ref'],'#diplome_select','code','libelle')
            complete_option(sorted_dip,'#diplome_select','code','libelle')

        })

    })

}

function process_data_async(data){

    var tmp_res = []
    var cpt = 0;

    var enfants = data['enfants']
    console.log(enfants)
    for (var i=0;i<enfants.length;i++){

        var e_enfants = enfants[i]['enfants']

        for(var j=0;j < e_enfants.length; j++){

            var c = e_enfants[j]['code']
            var v = e_enfants[j]['version']

            $.get('/db/proxy_etape/?code='+c+'&version='+v,function (data) {

                var tab_a = [{'code':data[0]['code']+':'+data[0]['version'],'libelle':data[0]['libelle']}]
                complete_option(tab_a,'#etape_select','code','libelle',false)

            })


        }

    }

}

function change_etape_retrieve(){

    $('body').on('change','#diplome_select',function(){

        $('#etape_select').find('option').not(':first').remove();


        var code = $('#diplome_select').val()

        $.get('/db/proxy_diplome_t/?code='+code,function(data){

            process_data_async(data)

        })

    })

}

function search_cb(value,node){

    return node.id.startsWith(value)

}


function select_etape_etp(){


    $('body').on('change','#etape_select',function(){

        var container_div = ''

        if ($('#etape_select').val() != '-- ETAPE --') {

            var cv = $('#etape_select').val().split(':')
            var code = cv[0]
            var version = cv[1]

            $.get('/db/proxy_etp_tree/?code='+code+'&version='+version,function(data){

                var js_tree = $('#js_tree').jstree(true)

                if (js_tree == false){

                    cfg_tree = { 'core' :
                                {'data': data,
                                'themes' : {'variant' : "large"}
                                },
                                'plugins': ["editmodule"],
                                }

                    container_div = $('#js_tree').jstree(cfg_tree)
                    container_div.on('loaded.jstree',function(){

                    container_div.jstree('open_all')

                    })



                }else{

                    var container_div = $('#js_tree').jstree(true)
                    $('#js_tree').jstree(true).settings.core.data = data
                    $('#js_tree').jstree(true).refresh(true)
                    $('#js_tree').on('changed.jstree',function(){

                        $('#js_tree').jstree('open_all')

                    })

                }


        })

        }


    })



}