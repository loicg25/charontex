$(document).ready(function() {

    $('.create-event').modalForm({
        formURL: "/db/add_token/"
    });

    $('.edit-token').each(function () {

        $(this).modalForm({
          modalID: "#modal-token",
          formURL: $(this).data('url') /* permet d'accèder au atribut data-* du html ici date-id */
        });
      });

});
