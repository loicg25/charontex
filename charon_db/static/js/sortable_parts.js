$(document).ready(function(){

    //partie création de la liste triable

    $('#sortable_parts').sortable();
    $( '#sortable_parts').disableSelection();

    //partie création du formulaire d'ajout
    $('#sub_type').empty()
    $('#sub_type').hide()
    $('#com_part').empty()
    $('#com_part').hide()
    $('#part_parameter').empty()
    $('#part_parameter').hide()
    $('#part_parameter_title').hide()
    var dialob_box = create_dialog()
    complete_select()
    register_add_button(dialob_box)

    enable_delete_item()


})


function enable_delete_item(){


    $('body').on('click','.trash-delete', function(){

        $(this).parent().remove()

    });

}

function generate_com_or_not(){

    var type = $('#type').val()

    if (type=='com'){

        var com_part = $('#com_part').val()

        if (com_part != "CREER UNE PARTIE"){

            return ':'+com_part

        }else{

            return ''

        }

    }else{

        return ''

    }

}

function generate_type(type,sub_type,title,param){

    if(sub_type == null){

        sub_type = '*'

    }else{

        sub_type = sub_type

    }

    if(param == ""){

        param = '*'

    }else{

        param = param

    }

    var com = generate_com_or_not()
    console.log('-- com --')
    console.log(com)
    var exist = false

    if((typeof com !== 'undefined') && com !=''){

        exist = already_exist_part(com.split(':')[1])

    }

    var tools_bar = go_edit(com)

    return [type+':'+sub_type+':'+title+':'+param+com,tools_bar,exist]
}

function go_edit(com){

    if ((typeof com !== 'undefined') && com !=''){

        return '<a target="_blank" href="/db/edit_part/'+com.split(':')[1]+'"><span class="pencil-edit oi oi-pencil">&nbsp;</span></a>'

    }else{

        return ''

    }

}

function reset_form(){

    $('#type').val('')
    $("#type").val($("#type option:first").val());
    $('#subtype').val('')
    $('#subtype').empty()
    $('#part_title').val('')
    $('#part_parameter').val('')

}

function already_exist_part(part_number){

    var inputs = $('input[name="parts[]"]')

    for(var i=0;i < inputs.length;i++){

        var id = $(inputs[i]).val().split(':')[4]

        if (id ==part_number){

            return true

        }

    }

    return false

}

function retrieve_all_part(){

    $.get('/db/rest_part_type/?type=com',function(data){

        $('#com_part').toggle();
        $('#com_part').empty();
        $('#com_part').append('<option>CREER UNE PARTIE</option>')

        for(var i=0;i< data.length;i++){

            $('#com_part').append('<option value="'+data[i]['id']+'">'+data[i]['titre']+'</option>')

        }


    })

}

function complete_select(){

    $('body').on('change','#type',function(){

        var val_type = $('#type').val()

        if (val_type == 'gen'){

            $('#sub_type').toggle();
            $('#sub_type').append('<option >-- SOUS TYPE --</option>')
            $('#sub_type').append('<option value="cou">Couverture</option>')
            $('#sub_type').append('<option value="cfi">Contact Filière</option>')
            $('#sub_type').append('<option value="exa">Calendrier d\'Examens</option>')
            $('#sub_type').append('<option value="cal">Calendrier général</option>')
            $('#sub_type').append('<option value="mcc">MCC</option>')  
            $('#sub_type').append('<option value="str">Structure</option>')
            $('#sub_type').append('<option value="mod">Module</option>')


        }else{

            $('#sub_type').empty()
            $('#sub_type').hide()
            $('#part_parameter').hide()
            $('#part_parameter').empty()
            $('#part_parameter_title').hide()

        }

        if (val_type == 'com'){

            //on va chargé les parties existant ou proposé d'en créer une part défaut + select2 si possible
            retrieve_all_part()

        }else{

            $('#com_part').empty()
            $('#com_part').hide()
            $('#part_title').prop('readonly',false)
            $('#part_title').val('')

        }

    })


    $('body').on('change','#sub_type',function(){

        $('#part_parameter').show()
        $('#part_parameter_title').show()

    })

    $('body').on('change','#com_part',function(){

        if ($('#com_part').val() != "CREER UNE PARTIE"){

            $('#part_title').val($('#com_part option:selected').text())
            $('#part_title').prop('readonly',true)

        }else{

            $('#part_title').prop('readonly',false)
            $('#part_title').val('')

        }

    })




}

function get_css_class(type){

    if(type == 'gen'){

        return 'item_sortable_red'

    }else if (type == 'gui'){

        return 'item_sortable_green'

    }else{

        return 'item_sortable_blue'

    }

}

function sub_str_title(val){

    var plus = ''

    if (val.length > 60){

        plus = ' ...'

    }

    return val.substr(0,60)+plus

}

function add_part(){



    var title = $('#part_title').val()
    var type=$('#type').val()
    var sub_type=$('#sub_type').val()
    var param = $('#part_parameter').val()
    var r_type = generate_type(type,sub_type,title,param)
    var css_class = get_css_class(type)

    if (title == '') {

        alert('Veuillez ajouter un titre à votre partie')

    }else{

        if (r_type[2]){

            alert("Cette partie est déja présente dans le guide")

        }else{


            var li= '<li class="'+css_class+' ui-state-default" style="width:100%;"><input type="hidden" name="parts[]" value="'+r_type[0]+'"/>'+sub_str_title(title)+'<span class="trash-delete oi oi-trash">&nbsp;</span>'+r_type[1]+'</li>'


            /* on ajout et on raffraichi la liste pour que ca s'affiche correctement */
            $("#sortable_parts").append(li)
            $("#sortable_parts").sortable('refresh')
            dialog.dialog( "close" )

        }


    }



}

/**
 * Fonction qui permet d'enregistrer l'ouverture au clic du formulaire de dialogue
 * @param dialog
 */
function register_add_button(dialog){

    $('body').on('click','#add_element',function(){

        dialog.dialog('open')
    })

}

/**
 *
 * Fonction qui crée la boite de dialogue dialog UI
 *
 * @returns {jQuery|*}
 */
function create_dialog(){

    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 500,
      width: 800,
      modal: true,
      buttons: {
        "Ajouter élément": add_part,
        "Annuler": function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        reset_form()
        $('#sub_type').empty()
        $('#sub_type').hide()
        $('#com_part').toggle();
        $('#com_part').empty();
        $('#part_title').prop('readonly',false)

        // allFields.removeClass( "ui-state-error" );
      }
    });

    form = dialog.find( "form" ).on( "submit", function( event ){
      event.preventDefault();
      add_part();
    });

    return dialog


}

