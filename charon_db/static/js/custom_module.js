function get_data(id,anu){

    var url = '/gh/module-declarations-rest/'+id+'/'+anu

    $.get(url,function(data){

        process_data(data)

    })

}

/* function that hide or show the save button if the year selected is the current one */
function hide_save(year){

    var url = '/gh/annee-courante-rest/'

    $.get(url,function(data){

        if (year == data['id']){

            $('#submit_form_button').show()

        }else{

            $('#submit_form_button').hide()

        }

    })

}

/* function that permit to update module annualised data */

function process_data(data){

    res ='<div>'
    res+='<div class="row"><div class="offset-md-1 col-md-5" style="text-align:center;border-bottom:1px solid black;">NOM</div><div class="col-md-3" style="text-align:center;border-bottom:1px solid black;">PRENOM</div><div class="col-md-1" style="text-align:center;border-bottom:1px solid black;">ETD</div><div class="col-md-1" style="text-align:center;border-bottom:1px solid black;"></div><div class="col-md-1" style="text-align:center;border-bottom:1px solid black;"></div></div>'

    for(var i=0;i<data.length;i++){

        // res+='<tr><td>&nbsp;'+data[i].module__code+'&nbsp;</td><td>&nbsp;'+data[i].module__libelle+'&nbsp;</td><td style="text-align:center;"><span class="editinput">&nbsp;'+data[i].etd+'&nbsp;</span></td><td>&nbsp;<span id="'+data[i].id+'" class="oi oi-pencil dec_edit">&nbsp;</span></td><td>&nbsp;<span id="'+data[i].id+'" class="oi oi-trash dec_delete">&nbsp;</span></td></tr>'
        res+='<div class="row"><div class="offset-md-1 col-md-5">&nbsp;'+data[i].intervenant__nom_patronymique+'['+data[i].intervenant__nom_usuel+']'+'&nbsp;</div><div class="col-md-3">&nbsp;'+data[i].intervenant__prenom+'&nbsp;</div><div class="col-md-1" style="text-align:center;"><span class="editinput">&nbsp;'+data[i].etd+'&nbsp;</span></div><div class="col-md-1">&nbsp;<span id="'+data[i].id+'" class="oi oi-pencil dec_edit">&nbsp;</span></div><div class="col-md-1">&nbsp;<span id="'+data[i].id+'" class="oi oi-trash dec_delete">&nbsp;</span></div></div>'

    }

    res+='</div>'

    $('#declaration_inter_rest').empty()
    $('#declaration_inter_rest').append(res)

}


function toggle_form_zone(){

    var statut = $('#module_zone .module_accordion_indicator').text()

    if (statut =='+'){

        $('.form_zone').show()
        $('#module_zone .module_accordion_indicator').text('-')
        // $('.button_forme_zone').show()

    }else{

        $('.form_zone').hide()
        $('#module_zone .module_accordion_indicator').text('+')
        // $('.button_forme_zone').hide()
    }


}

function toggle_inter_zone(){

    var statut = $('#inter_zone .module_accordion_indicator').text()

    if (statut =='+'){

        $('.enseignant_dec_zone').show()
        $('#inter_zone .module_accordion_indicator').text('-')
        $('.button_inter_zone').show()
        $('.enseignant_dec_zone').show()
        $('#declaration_inter_rest').show()


    }else{

        $('.enseignant_dec_zone').hide()
        $('#inter_zone .module_accordion_indicator').text('+')
        $('.button_inter_zone').hide()
        $('.enseignant_dec_zone').hide()
        $('#declaration_inter_rest').hide()

    }


}

function change_name(name,key){

    // var inc = name.split('[')[1].split(']')[0]
    var key_var = key+'[name^="'+name.split('[')[0]+'["]'
    var inc = get_max_inc(key_var)
    var name = name.split('[')[0]
    inc = parseInt(inc)
    inc +=1
    return name+'['+inc+']'

}
function get_max_inc(key_var){

    var l_elt = $(key_var)
    var res_inc = 0

    for(var i=0;i<l_elt.length; i++){

        var inc = $(l_elt[i]).attr('name').split('[')[1].split(']')
        console.log(res_inc)
        if (parseInt(inc) > parseInt(res_inc)){

            res_inc = inc

        }
    }

    return res_inc

}

function append_more(test,elt){

    if(test =='+'){

        var v = $('#add_enseignant_dec_zone').clone()
        v.find('.select2').remove()
        $(elt).find('.circle_button').text('-')
        var teach = v.find('#teacher-select')
        teach.attr('name',change_name(teach.attr('name'),'#teacher-select'))
        var cm = v.find('#CM')
        cm.val('')
        cm.attr('name',change_name(cm.attr('name'),'#CM'))
        var td = v.find('#TD')
        td.val('')
        td.attr('name',change_name(td.attr('name'),'#TD'))
        var tp = v.find('#TP')
        tp.attr('name',change_name(tp.attr('name'),'#TP'))
        tp.val('')
        v.find('.circle_button').text('+')
        v.find('#teacher-select').removeClass("select2-hidden-accessible")
        // var btn = $('#btn_teacher').clone() //validé
        // $('#btn_teacher').remove()
        $('.enseignant_dec_zone').append(v)
        // $('#content_form').append(btn)
        $('.teacher_select').select2();

    }else{

        $(elt).remove()

        var btn = $('#btn_teacher').clone()
        $('#btn_teacher').remove()
        $('#content_form').append(btn)

    }

}

function delete_declaration(id,elt){

    var url = '/gh/delete-declarations-rest/'+id
    $.get(url,function(data){

    })
    $(elt).remove()
    update_data()


}

function validate_change(id,new_value){

    console.log('ici')
    console.log(new_value)
    var url = '/gh/update-declarations-rest/'+id+'/'+new_value.trim()
    console.log(new_value)
    $.get(url)
    update_data()


}

function change_input(elt,clicked){

    var type = $(elt)[0].tagName


    if (type =='SPAN'){

        var value =$(elt).text()
        elt.replaceWith('<input  class="col-md-8 editinput" value="'+value+'" />')
        $(clicked).toggleClass('oi-pencil oi-check')
        $(clicked).addClass('update_dec_tmp')

    }else{

        var value =$(elt).val()
        elt.replaceWith('<span class="editinput">'+value+'<span/>')
        $(clicked).toggleClass('oi-check oi-pencil')
        var id = $(clicked).attr('id')
        validate_change(id,value)

    }

}


function getModuleData(){

    var code = $('#vcode_recap_zone').text()
    var anu = $('#year_zone_select').val()

    var url = '/gh/module-data-rest/'+anu+'/'+code

    $.get(url,function(data){

        if (data['fitest'] == true){

          $("#id_fitest").prop('checked',true)

        }else{


            $("#id_fitest").prop('checked',false)

        }

        $("#id_flux").val(data['flux'])
        $("#id_flux_fitest").val(data['flux_fitest'])
        update_data()

    })

}


function construct_array(){

    var code = $("#id_code").val()
    var cm = $("#id_cm").val()
    var td = $("#id_td").val()
    var tp = $("#id_tp").val()
    var fitest = $("#id_fitest").prop('checked')
    var type = $("#id_type").val()
    var flux = $("#id_flux").val()
    var flux_fitest = $("#id_flux_fitest").val()

    return {'code':code,'cm':cm,'td':td,'tp':tp,'fitest':fitest,'type':type,'flux':flux,'flux_fitest':flux_fitest}

}


/* function that will construct the list of pre dec key used and give key to find cm td tp value for each new declaration */
function construct_key(){

    var teachers = $('#teacher-select[name^="teacher["]')
    var array_key = []

    for(var i=0; i<teachers.length ; i++){

        var name = $(teachers[i]).attr('name')

        if (typeof name != 'undefined'){

            array_key[i] = {'key': name , 'ind': $(teachers[i]).attr('name').split('[')[1].split(']')[0]}
        }


    }

    return array_key

}

function v_o_z(val){

    if (val !=''){

        return val

    }else{

        return 0

    }

}

function construct_dec(){

    var keys = construct_key()
    var array_dec = []

    for(var i=0;i<keys.length;i++){

        var teacher = $('#teacher-select[name="'+keys[i]['key']+'"]').val()

        if (teacher != '-- ENSEIGNANT --'){

            array_dec[i] = {'teacher':teacher,
            'cm':v_o_z($('#CM[name="cm['+keys[i]['ind']+']"]').val()),'td':v_o_z($('#TD[name="td['+keys[i]['ind']+']"]').val()),'tp':v_o_z($('#TP[name="tp['+keys[i]['ind']+']"]').val())}

        }


    }

    return array_dec
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function apply_charte(infos,decs){

    var url = '/gh/charte-calc-rest/'
    var data = {'decs': JSON.stringify(decs),'infos':JSON.stringify(infos)}

    // var data = {'decs': 1,'infos':2}

    var csrftoken = $("[name=csrfmiddlewaretoken]").val();

    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
    });
    $.support.cors = true;

    $.post(url,data,function(data){

        $('#vhtheo_recap_zone').text(data['eth'])
        $('#vhreel_recap_zone').text(data['er'])

    })

}


/* function that will construct to array of data according to the data present on the module forms and declaration added  */
function update_data(){

    /* first we get all form data about the module */
    var data_array = construct_array()
    /* then we collect each new declaration */
    var decs = construct_dec()
    /* then we send those data to ws for calculation */
    var res = apply_charte(data_array,decs)
    /* finally we update every form */

}


$(document).ready(function(){

    console.log('prune')

    if ($('#year_zone_select option:selected').text() != '-- ANNEE --'){

        var id_module = $('#vcode_recap_zone').text()
        var anu = $('#year_zone_select').val()
        get_data(id_module,anu)

    }



    $('body').on('click','.dec_delete',function(){

        var id = $(this).attr('id')
        var parent = $(this).parent().parent()

        delete_declaration(id,parent)


    })

    $('body').on('click','.dec_edit',function(){


        var id = $(this).attr('id')
        var parent = $(this).parent().parent()
        var input = $(parent).find('.editinput')
        change_input(input,this)

    })


    $('body').on('click','.circle_button',function(){

        var test = $(this).text()
        var elt = $(this).parent().parent()

        append_more(test,elt)
        update_data()

    })

    $('body').on('change','#year_zone_select',function(){

        var id_module = $('#vcode_recap_zone').text()
        var anu = $('#year_zone_select').val()
        getModuleData()
        hide_save(anu)
        get_data(id_module,anu)

    })

    $('.form_zone').hide()
    // $('.button_forme_zone').hide()

    $("#module_zone.info_module_clic").on('click',function(){

        toggle_form_zone()


    })

    $("#inter_zone.info_module_clic").on('click',function(){

        toggle_inter_zone()


    })

    /* every time we add a new data the data are updated */
    $('body').on('input','#id_flux, #id_flux_fitest, #id_td, #id_cm, #id_tp, #id_fitest, #id_type, #id_niveau, #CM, #TD, #TP, #select2-teacher-select-container',function(){

        update_data()

    })

    $('body').on('change',"#id_fitest",function(){

        update_data()

    })


    $('.teacher_select').select2();
    $('#year_zone_select').select2();
    $('.enseignant_dec_zone').hide();
    $('#declaration_inter_rest').hide()

})