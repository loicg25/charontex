function serialize_form(){

    var debut = $('#id_date_debut_examen').val()
    var fin = $('#id_date_fin_examen').val()
    var salle = $('#id_salle_examen').val()
    var debutr = $('#id_date_debut_examen_r').val()
    var finr = $('#id_date_fin_examen_r').val()
    var saller = $('#id_salle_examen_r').val()


    var p1 = $('#id_examen_periode1').is(':checked')
    var p2 = $('#id_examen_periode2').is(':checked')

    return {'debut':debut,'fin':fin,'salle':salle,'debutr':debutr,'finr':finr,'saller':saller,'p1':p1,'p2':p2,'id':$('#modal_id_up').text()}

}


function send_form(){

    var arr_form = serialize_form()

    $.post('/db/update_calendar_modal/',arr_form,function(data){

        if(data=='OK'){

            $('#dialog_for_modal').dialog('close')

        }else{

            $('#dialog_for_modal').html('Erreur de traitement')

        }

    })


}


function create_dialog(){

    $("#dialog_for_modal").dialog({
        resizable: true,
        autoOpen: false,
        width: 800,
        modal: true,
        buttons: {
          'Fermer': function() {
            $(this).dialog('close');
          },
           'Valider': send_form,
       }
   });


}


$(document).ready(function(){

        create_dialog()

        $('body').on('click','.js_tree_edit_link',function(){
            var id = $(this).attr('href').split('/')[3]
            var id_hidde = '<span id="modal_id_up" style="display:none;">'+id+"</span>"
            console.log(id)
            $.get('/db/n_calendar_rest/'+id,function(data){

                $('#dialog_for_modal').html(data+id_hidde)
                $('#dialog_for_modal').dialog('open')

            })

        })

    }
)