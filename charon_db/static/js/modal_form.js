$(document).ready(function() {

    $('.create-event').modalForm({
        formURL: "/db/add_event_calendar/"
    });

    $('.edit-event').each(function () {

        $(this).modalForm({
          modalID: "#modal-event",
          formURL: $(this).data('url') /* permet d'accèder au atribut data-* du html ici date-id */
        });
      });

});
