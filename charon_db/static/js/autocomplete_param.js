/**
 *
 * Fonction qui prend en charge les différents type de champs paramètres à auto compléter et créer la bonne liste en fonction de la saisie utilisateur
 * Si ce dernier ne type pas le champs il ne se passera rien
 *
 */



$(document).ready(function(){


    // on va rendre le champs paramètre sensible à l'autocompletion de JQUERRY UI
    $('#part_parameter').autocomplete({

        source : function(request,response){

            var type = $('#sub_type option:selected').val()

            if (type =='cfi'){

                $.ajax({

                    url: '/db/proxy_filiere/',
                    dataType : 'json',
                    success : function(data){

                        response($.map(data,function(item) {

                            var val = $('#part_parameter').val().toLowerCase()

                            if (item.libelle.toLowerCase().indexOf(val) != -1) {

                                return {'label': item.libelle, 'value': item.id}

                            }
                        }))

                    }

                })

            }else{

                $.ajax({

                    url: '/db/proxy_etapes/',
                    dataType : 'json',
                    success : function(data){

                        response($.map(data,function(item) {

                            var val = $('#part_parameter').val().toLowerCase()

                            if (item.libelle.toLowerCase().indexOf(val) != -1) {

                                return {'label': item.libelle, 'value': item.code + "-" + item.version}

                            }
                        }))

                    }

                })

            }

        }

    })


})