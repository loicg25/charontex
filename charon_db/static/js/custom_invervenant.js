
function get_data(id,anu){

    var url = '/gh/declarations-rest/'+id+'/'+anu

    $.get(url,function(data){

        process_data(data)

    })

}

function check_data(data,name){

    if (data[name] == true){

        return 'checked'

    }else{

        return ''

    }

}

function process_data(data){

    res ='<div>'
    res+='<div class="row"><div class="offset-md-1 col-md-1" style="text-align:center;border-bottom:1px solid black;">CODE</div><div class="col-md-4" style="text-align:center;border-bottom:1px solid black;">LIBELLE</div><div class="col-md-3" style="text-align:center;border-bottom:1px solid black;"></div><div class="col-md-1" style="text-align:center;border-bottom:1px solid black;"></div><div class="col-md-1" style="text-align:center;border-bottom:1px solid black;"></div></div>'

    for(var i=0;i<data.length;i++){
        console.log(data[i])
        // res+='<tr><td>&nbsp;'+data[i].module__code+'&nbsp;</td><td>&nbsp;'+data[i].module__libelle+'&nbsp;</td><td style="text-align:center;"><span class="editinput">&nbsp;'+data[i].etd+'&nbsp;</span></td><td>&nbsp;<span id="'+data[i].id+'" class="oi oi-pencil dec_edit">&nbsp;</span></td><td>&nbsp;<span id="'+data[i].id+'" class="oi oi-trash dec_delete">&nbsp;</span></td></tr>'
        res+='<div class="row"><div class="offset-md-1 col-md-1">&nbsp;'+data[i].module__code+'&nbsp;</div><div class="col-md-4">&nbsp;'+data[i].module__libelle+'&nbsp;</div><div class="col-md-1" style="text-align:center;"><span class="editinput editableinput-'+data[i].id+'">&nbsp;'+data[i].etd+'&nbsp;</span></div><div><label>Co:&nbsp;<input id="'+data[i].id+'" name="co" class="malus" '+check_data(data[i],'m_correction')+' type="checkbox">&nbsp;</label><label>De:&nbsp;<input id="'+data[i].id+'" name="de" '+check_data(data[i],'m_devoir')+' class="malus" type="checkbox">&nbsp;</label><label>Do:&nbsp;<input id="'+data[i].id+'" name="do" '+check_data(data[i],'m_document')+' class="malus" type="checkbox">&nbsp;</label><label>Ex:&nbsp;<input id="'+data[i].id+'" class="malus" name="ex" '+check_data(data[i],'m_exercice')+' type="checkbox">&nbsp;</label><label>Ec:&nbsp;<input id="'+data[i].id+'" class="malus" name="ec" '+check_data(data[i],'m_echange')+' type="checkbox">&nbsp;</label><br/><label>Fo:&nbsp;<input id="'+data[i].id+'" name="fo" '+check_data(data[i],'m_forum')+' class="malus" type="checkbox">&nbsp;</label><label>Pa:&nbsp;<input id="'+data[i].id+'" name="pa" '+check_data(data[i],'m_parcours')+' class="malus" type="checkbox">&nbsp;</label><label>Pc:&nbsp;<input id="'+data[i].id+'" name="pc" '+check_data(data[i],'m_pcours')+' class="malus" type="checkbox">&nbsp;<label>Pi:&nbsp;<input name="pi" id="'+data[i].id+'" class="malus" '+check_data(data[i],'m_pinter')+' type="checkbox">&nbsp;</label></label><label>Tu:&nbsp;<input id="'+data[i].id+'" class="malus" name="tu" '+check_data(data[i],'m_tutorat')+' type="checkbox"></label>&nbsp;</div><div class="col-md-1">&nbsp;<span id="'+data[i].id+'" class="oi oi-pencil dec_edit">&nbsp;</span></div><div class="col-md-1">&nbsp;<span id="'+data[i].id+'" class="oi oi-trash dec_delete">&nbsp;</span></div></div>'

    }

    res+='</div>'

    $('#enseignant_container').empty()
    $('#enseignant_container').append(res)

}


function append_more(test,elt){

    if(test =='+'){

        var v = $('#add_enseignant_container').clone()
        v.find('.select2').remove()
        $(elt).find('.circle_button').text('-')
        var teach = v.find('#cours-select')
        teach.attr('name',change_name(teach.attr('name')))
        var cm = v.find('#CM')
        cm.val('')
        cm.attr('name',change_name(cm.attr('name')))
        var td = v.find('#TD')
        td.val('')
        td.attr('name',change_name(td.attr('name')))
        var tp = v.find('#TP')
        tp.attr('name',change_name(tp.attr('name')))
        tp.val('')
        v.find('.circle_button').text('+')
        v.find('#cours-select').removeClass("select2-hidden-accessible")
        var btn = $('#btn_teacher').clone()
        $('#btn_teacher').remove()
        $('.content_add_enseignant').append(v)
        $('#content_form').append(btn)
        $('.teacher_select').select2();

    }else{

        $(elt).remove()

        var btn = $('#btn_teacher').clone()
        $('#btn_teacher').remove()
        $('#content_form').append(btn)

    }

}

function change_name(name){

    var inc = name.split('[')[1].split(']')[0]
    var name = name.split('[')[0]
    inc = parseInt(inc)
    inc +=1

    return name+'['+inc+']'

}

function delete_declaration(id,elt){

    var url = '/gh/delete-declarations-rest/'+id
    $.get(url,function(data){
        console.log(data)
        alert(data);

    })
    $(elt).remove()

}

function validate_change(id,new_value){

    var url = '/gh/update-declarations-rest/'+id+'/'+new_value.trim()
    $.get(url)

}

function change_input(elt,clicked){

    var type = $(elt)[0].tagName


    if (type =='SPAN'){

        var value =$(elt).text()
        elt.replaceWith('<input class="editinput" value="'+value+'" />')
        $(clicked).toggleClass('oi-pencil oi-check')
        $(clicked).addClass('update_dec_tmp')

    }else{

        var value =$(elt).val()
        elt.replaceWith('<span class="editinput">'+value+'<span/>')
        $(clicked).toggleClass('oi-check oi-pencil')
        var id = $(clicked).attr('id')
        validate_change(id,value)
    }

}

function test_dec(){


    if($('#CM').val() =='' && $('#TD').val() =='' && $('#TP').val() ==''){

        return true

    }else{

        return false

    }

}

function get_identity(elt){

    return {'id':$(elt).attr('id'),'name':$(elt).attr('name'),'value':$(elt).is(':checked')}


}

function send_malus(t_id){

    url = 'update-malus-rest/'+t_id['id']+'/'+t_id['value']+'/'+t_id['name']
    $.get(url)

}

function decrease_increase(t_id){

    var o_val = $('.editableinput-'+t_id['id']).text()

    if (t_id['value'] == true){

        var n_val = parseFloat(o_val) - 3


    }else{

        var n_val = parseFloat(o_val) + 3

    }
    $('.editableinput-'+t_id['id']).text(n_val)
    validate_change(t_id['id'],n_val)

}


$(document).ready(function(){


    $('#teacher-select').on('select2:select',function(){

        var teacher = $('#teacher-select').val()
        var annee = $('#year').val()


        get_data(teacher,annee)

    });

    $('body').on('click','.circle_button',function(){

        var test = $(this).text()
        var elt = $(this).parent().parent()

        append_more(test,elt)

    })

    $('body').on('click','.dec_delete',function(){

        var id = $(this).attr('id')
        var parent = $(this).parent().parent()

        delete_declaration(id,parent)

    })

    $('body').on('click','.dec_edit',function(){


        var id = $(this).attr('id')
        var parent = $(this).parent().parent()
        var input = $(parent).find('.editinput')
        change_input(input,this)

    })

    $('body').on('change','.malus',function(){

        var val = get_identity(this)
        decrease_increase(val)
        send_malus(val)

    })


    $('#content_form').submit(function(event){


        if ($('#teacher-select').val() == '-- ENSEIGNANTS --' || $('#year').val() == "-- ANNEE --" || $('#cours-select').val() == "-- MODULE --" ){

            event.preventDefault()
            alert('Merci de complète les zone : enseignants, déclaration et année')

        }else if (test_dec()){

            event.preventDefault()
            alert('Merci d\'indiquer un nombre d\'heure')

        }
        else{

            return ;

        }


    })
    


})