


$(document).ready(function(){

    $('#id_nom_usuel').autocomplete({

    source : function(typed,returned){

    var tmp_url = '/gh/proxy-ens/'
    var url = tmp_url+typed.term

    $.get(url,function (data){

        returned(data);
    })

    },
    minLength: 2,

    select: function( event, ui ) {

        $( "#id_nom_usuel" ).val( ui.item.sNOM_USUEL );
        $( "#id_nom_patronymique" ).val( ui.item.sNOM_PATRO );
        $( "#id_prenom" ).val( ui.item.sPrenom );
        $( "#id_email" ).val( ui.item.sEMAIL );
        $( "#id_composante" ).val( ui.item.sUFRCODE );
        $( "#id_code_enseignant" ).val( ui.item.sENCODE );


        return false;
    }

    }).autocomplete("instance")._renderItem = function(ul,item){

        return $("<li>").append("<div>"+item.sNOM_PATRO+'('+item.sNOM_USUEL+')'+" "+item.sPrenom+"</div>").appendTo(ul)

    };

})