(function ($, undefined) {
	"use strict";

	var inp = document.createElement("A");
	inp.innerHTML ="<span class='oi oi-pencil'>&nbsp;</span>"
	inp.className = "jstree-edit_link jstree-edit_link";

	$.jstree.defaults.editmodule = {};

	$.jstree.plugins.editmodule = function (options, parent) {
		this.bind = function () {
			parent.bind.call(this);
			this._data.editmodule.uto = false;
			this.element
				.on('changed.jstree uncheck_node.jstree check_node.jstree uncheck_all.jstree check_all.jstree move_node.jstree copy_node.jstree redraw.jstree open_node.jstree ready.jstree loaded.jstree', $.proxy(function () {
						// only if undetermined is in setting
						if(this._data.editmodule.uto) { clearTimeout(this._data.editmodule.uto); }
						this._data.editmodule.uto = setTimeout($.proxy(this._editmodule, this), 50);
					}, this));
		};
		this.redraw_node = function(obj, deep, callback, force_draw) {

		    var identifier = obj

			obj = parent.redraw_node.call(this, obj, deep, callback, force_draw);

			if(obj) {
				var i, j, tmp = null, chk = inp.cloneNode(true);
				for(i = 0, j = obj.childNodes.length; i < j; i++) {
					if(obj.childNodes[i] && obj.childNodes[i].className && obj.childNodes[i].className.indexOf("jstree-anchor") !== -1) {
                        tmp = obj.childNodes[i];

                        var node = $('.jstree').jstree(true).get_json(identifier)
                        var type_node = node.data['type']
                        console.log(type_node)

                        if (tmp.href != ""){

                            if (type_node == 'UE' || type_node =='ELC'|| type_node =='MATI' || type_node =='OPT'  || type_node == 'UEST' || type_node == 'STAG' || type_node == 'UEPR' || type_node=='PRJ' || type_node == 'CHOI'){

                                tmp.removeAttribute('href');
                                tmp.innerHTML += '&nbsp;&nbsp;&nbsp;<a class="js_tree_edit_link" href="/db/edit_module/'+identifier.split('_')[0]+'"><span class="oi oi-pencil">&nbsp;</span></a>'

								break;

                            }


                        }
					}
				}

			}
			return obj;
		};
		this._editmodule = function () {
			var ts = this.settings.checkbox.tie_selection;

			$('.jstree-edit_link').each(function () {
				this.checked = (!ts && this.parentNode.parentNode.className.indexOf("jstree-edit_link") !== -1) || (ts && this.parentNode.parentNode.className.indexOf('jstree-clicked') !== -1);
				this.indeterminate = this.parentNode.className.indexOf("jstree-undetermined") !== -1;
				this.disabled = this.parentNode.parentNode.className.indexOf("disabled") !== -1;
			});
		};
	};
})(jQuery);