from django.contrib.auth.middleware import PersistentRemoteUserMiddleware

HASH_SESSION_KEY = '_auth_user_hash'

class CustomMiddleWare(PersistentRemoteUserMiddleware):

    header = 'HTTP_CAS_USER'