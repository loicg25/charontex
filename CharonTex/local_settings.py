STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',

]

import os

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')
MEDIA_ROOT = '/static_tel/'
MEDIA_URL = '/static_tel/'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'format_tags': 'p;h1;h2;h3;h4;h5;h6;pre',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline', 'Subscript', 'Superscript', 'TextColor'],
            ['Format'],
            ['NumberedList', 'BulletedList'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink', 'Image'],
            ['Table', 'PageBreak'],
            ['FMathEditor', 'Latex'],
            # On indique l'endroit où le bouton du plugin LaTeX doit apparaître dans l'éditeur
            ['Source']
        ],
        # On ajoute le plugin LaTeX à l'éditeur
        'extraPlugins': 'FMathEditor, Latex',
        'allowedContent': True
    }
}

COD_COMPOSANTE = '926'
FILIERE = ""
DIPLOME_T = ""
DIPLOME = ""
ETAPE = ""
ETAPES = ''
MODULE = ""
TREE = "https://run.mocky.io/v3/1a203895-5a9b-44dd-8648-a8d62abebdae/"
ELP_ETAPE = ""
FILIERE_DIPLOME = ""
FILIERE_INFO = ""
FILIERE_ETAPE = ""
GET_CONTACT = ""
GET_CONTACT_COMPOSANTE = "https://run.mocky.io/v3/8f6cdd9e-9685-4274-925d-2d3c9dbd40ed/"
GET_CONTACT_FIL = "https://run.mocky.io/v3/074acc12-391d-44dd-a040-95e0b88da1f9/"
GET_FILIERE = "https://run.mocky.io/v3/8cf9930d-fddc-4089-bfbe-ac0480318f9c/"
MAQ_EXAM = ''

from django.conf.global_settings import DATETIME_INPUT_FORMATS

DATETIME_INPUT_FORMATS += ("%d/%m/%Y - %H:%M",)

LOGOUT_REDIRECT_URL = '/'

GEN_HEADER_DUPLICATA = '/CharonTex/templates/header_duplicata.html'

from .settings import INSTALLED_APPS

INSTALLED_APPS += ''

DEBUG = True
