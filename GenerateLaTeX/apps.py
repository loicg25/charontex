from django.apps import AppConfig


class GeneratelatexConfig(AppConfig):
    name = 'GenerateLaTeX'
