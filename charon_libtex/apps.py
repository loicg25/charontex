from django.apps import AppConfig


class CharonLibtexConfig(AppConfig):
    name = 'charon_libtex'
