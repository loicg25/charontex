from .concat_pdf import ConcatPdf
from .part_converter import *
import os
from datetime import date, datetime
from django_tex.core import compile_template_to_pdf
from pdflatex import PDFLaTeX
import pathlib
import io
from django.conf import settings
from django.template import Template, loader
import fileinput

"""

Libs that permit to generate a guide from data inside Charon

"""


class GuideGeneratorTex(object):
    guide = None

    def __init__(self, guide):

        self.guide = guide

    # Fonction permettant de construire un guide avec l'ensemble des parties (Header, Footer, Sommaire, Contenue...
    def process_part(self, guide, brouillon):
        result = []
        as_couv = False
        ordered_guide_list = sorted(guide.parti.all(),key=lambda p: p.ordrepartiguide_set.filter(guide=guide).last().numero_ordre)
        # Génération du header et du sommaire
        today = date.today()
        d1 = today.strftime("%d/%m/%Y")
        val = {'duplicata': brouillon, 'date': d1, 'filiere': guide}
        conv = ConvertPart(val)
        header = conv.render_template("header.tex", val)
        result.append(header)
        sommaire = conv.render_template("sommaire.tex", {})
        result.append(sommaire)
        # Génération des parties du guide et verifie la disponibilité d la couverture
        for part in ordered_guide_list:
            if part.type_parti == 'gen' and part.sous_type == 'cou':
                as_couv = True
            else:
                conv = ConvertPart(part)
                processed_part = conv.convert()
                result.append(processed_part)
        # Génération du footer
        footer = conv.render_template("footer.tex", {})
        result.append(footer)
        return result, as_couv

    # Permet de concatener toutes les sections des codes LaTeX à partir des templates
    def concat_tex(self, tex_parts):
        res = ''
        for tex in tex_parts:
            if tex is not None:
                res += tex
        return res

    # Fonction principale permettant la génération d'un guide avec la compilation
    def build(self, brouillon=False):

        # Génération des différentes parties du guide en utilisant des templates
        latex_parts, as_couv = self.process_part(self.guide, brouillon)
        #Concaténation des différentes parties des guides
        latex_concat = self.concat_tex(latex_parts)

        #Date de génération utilisé pour nommé le document lors de la génération jusqu'à la microseconde
        now = datetime.now()
        d1 = now.strftime('%Y_%m_%d_%H_%M_%S_%f')

        # Ecriture du code latex généré dans un fichier pour être ensuite compilé
        f = open('./generated_docs/preview' + d1 + '.tex', 'w', encoding='utf-8')
        f.write(latex_concat)
        f.close()

        #Double compilation du fichier LaTeX (permet de générer la table des matières) pour obtenir un fichier PDF
        os.system('pdflatex -interaction=nonstopmode -aux-directory=generated_docs -output-directory=generated_docs ./generated_docs/preview' + d1 + '.tex')
        os.system('pdflatex -interaction=nonstopmode -aux-directory=generated_docs -output-directory=generated_docs ./generated_docs/preview' + d1 + '.tex')

        #Suppression des fichiers de génération dans le repertoire generated_docs pour libérer de la mémoire
        os.remove('./generated_docs/preview' + d1 + '.aux')
        os.remove('./generated_docs/preview' + d1 + '.log')
        os.remove('./generated_docs/preview' + d1 + '.out')
        os.remove('./generated_docs/preview' + d1 + '.toc')
        os.remove('./generated_docs/preview' + d1 + '.tex')
        f = open('./generated_docs/preview' + d1 + '.pdf', 'r', encoding='utf-8')

        #Supression des images généré à partir d'une base64 en png lors de la génération du document
        import glob
        for i in glob.glob(os.path.join("generated_docs", "*.png")):
            try:
                os.chmod(i, 0o777)
                os.remove(i)
            except OSError:
                pass

        #Si une couverture est disponible, concaténation de la couverture avec le document généré
        if as_couv:
            couverture = self.guide.couverture.file
            pdf_file = ConcatPdf.concat_pdf(couverture,'./generated_docs/preview'+d1+'.pdf')
            pdf_file = './generated_docs/preview' + d1 + '.pdf'
        else:
            pdf_file = './generated_docs/preview' + d1 + '.pdf'
        return pdf_file
