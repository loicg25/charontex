from django.template import Template, Context

class TextTransform(object):
    '''

    Fonction qui transforme fait la substitution dans un texte (text) des chevrons à l'aide de token_list pour obtenir les valeurs
    text : une chaine de caractère
    token_list : un dictionnaire clé, valeur

    :return une chaine de caractère ou les valeurs sont substituées
    '''

    @staticmethod
    def text_transform(text, token_list):
        t = Template(text)  # transform le text en template
        c = Context(token_list)  # le contenu doit etre un dictionnaire
        return t.render(c)
