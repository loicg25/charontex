import datetime

"""

Calcul de la date de debut iso pour l'année donné
Principe : On part d'une date connu qui est obligatoirement dans l'année qui nous interesse (4 janvier ideal car elle est toujours la première semaine du calendrier iso) on le converti en iso on calcul le delta avec notre autre iso
et on ajoute ce delta a notre date gregorienne. Du coup le delta étant identique entre iso et gregorien on a convertir de iso a gregorien.
    
"""


def iso_to_gregorian(iso_year, iso_week, iso_day):
    debut = datetime.date(iso_year, 1,
                          4)  # on part du 4 janvier de l'année souhaité car toujours dans le calendrier, date gregorienne
    vide, debut_semaine, debut_jour = debut.isocalendar()  # on renvoi directement le trio sur 3 variables,on converti en iso

    return debut + datetime.timedelta(days=iso_day - debut_jour, weeks=iso_week - debut_semaine)
