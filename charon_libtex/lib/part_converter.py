# -*- coding: utf-8 -*-
from datetime import time

from django.template import Template, loader
from charon_db.models import GeneralCalendar, Annee, Module, OrdrePartiGuide, KeyValue
import requests
import html
from django.conf import settings
import json
from django_tex.shortcuts import render_to_pdf
from django_tex.core import compile_template_to_pdf
import htmlmin

from .parserHTML import MyHTMLParser
from .substituate import *

"""

Lib that permit to create html for all kind of part in charon

"""


class ConvertPart(object):
    part = None

    def __init__(self, part):
        self.part = part

    # Permet le remplacement des caractères possèdant des accents et autres variations de lettres
    def supprime_accent(self, chaine):
        """ supprime les accents du texte source """
        out = ""
        for mot in chaine:
            for c in mot:
                if c == 'é' or c == 'è' or c == 'ê' or c == 'ë':
                    c = 'e'
                elif c == 'à' or c == 'â' or c == 'ä':
                    c = 'a'
                elif c == 'ù' or c == 'û' or c == 'ü':
                    c = 'u'
                elif c == 'î' or c == 'ï' or c == 'ì':
                    c = 'i'
                elif c == 'ç':
                    c = 'c'
                elif c == 'ô' or c == 'ö':
                    c = 'o'
                out += c
        return out

    # Permet de créer le format de l'ancre
    def create_format_ancre(self, chaine):
        str = chaine.lower()
        strWhitoutVirgule = str.replace(',', '')
        strWhitoutAppos = strWhitoutVirgule.replace("'", '')
        strWhitoutAppo2 = strWhitoutAppos.replace("’", '')
        strWhitoutTwoPoint = strWhitoutAppo2.replace(':', '')
        strWhitoutPar1 = strWhitoutTwoPoint.replace('(', '')
        strWhitoutPar2 = strWhitoutPar1.replace(')', '')
        strWhitoutEsp = strWhitoutPar2.replace('&', '')
        strWhitoutDie = strWhitoutEsp.replace('#', '')
        strWhitoutAccent = self.supprime_accent(strWhitoutDie)
        str2 = strWhitoutAccent.title()
        return str2.replace(" ", '')

    # création de la liste des token présents dans le texte
    def create_token_list(self):
        res = {}
        tokens = KeyValue.objects.filter(annee_guide=Annee.objects.get(current=True))
        for token in tokens:
            res.update({'ctu_' + str(token.key): str(token.value)})
        return res

    # Protection des symboles et caractères spéciaux en LaTeX
    def protectAndUnescapeText(self, text):
        ut = html.unescape(text)
        protectEsper = ut.replace("&", "\&")
        protectPourc = protectEsper.replace("%", "\%")
        protectDie = protectPourc.replace("#", "\#")
        protectUnde = protectDie.replace("_", "\_")
        protectDollar = protectUnde.replace("$", "\$")
        return protectDollar

    # Génération du code pour le fichier LaTeX final
    def prepare_text(self, text):
        parser = MyHTMLParser()
        # Création des tokens
        tokens = self.create_token_list()
        f_text = TextTransform.text_transform(text, tokens)
        # protection du texte (après la création des tokens pour ne pas perturber cette dernière puisque les tokens utilisent des {})
        protectedText = self.protectAndUnescapeText(f_text)
        # Appel du parser avec htmlmin.minify pour supprimer tous les sauts de lignes et espaces entre les balises HTML (pour ne pas avoir des espaces non désirés dans le LaTeX)
        parser.feed(htmlmin.minify(protectedText, remove_comments=True, remove_empty_space=True))
        return parser.getOutput()

    def val_or_empty(self, val):
        try:
            return val[0]
        except Exception:
            return None

    def get_filiere_data(self, filiere_id):

        url = settings.GET_FILIERE + str(filiere_id)
        req = requests.get(url=url)
        filiere = req.json()

        return filiere

    # Récupération des infos de contact du centre
    def get_contact_centre(self):
        url = settings.GET_CONTACT_COMPOSANTE + "/resp/"
        url2 = settings.GET_CONTACT_COMPOSANTE + "/rsa/"
        url3 = settings.GET_CONTACT_COMPOSANTE + "/dir/"
        url4 = settings.GET_CONTACT_COMPOSANTE + "/dira/"
        req = requests.get(url=url)
        req2 = requests.get(url=url2)
        req3 = requests.get(url=url3)
        req4 = requests.get(url=url4)
        contact_resp = req.json()
        contact_rsa = req2.json()
        contact_dir = req3.json()
        dira = req4.json()

        return {'dir': self.val_or_empty(contact_dir), 'resp': self.val_or_empty(contact_resp),
                'rsa': self.val_or_empty(contact_rsa), 'dira': self.val_or_empty(dira)}

    # Récupération des infos de contact de la filière
    def get_contact_filiere(self, parametre):

        url = settings.GET_CONTACT_FIL + str(parametre) + "/scol/"
        url2 = settings.GET_CONTACT_FIL + str(parametre) + "/resf/"
        req = requests.get(url=url)
        req2 = requests.get(url=url2)
        contact_scol = req.json()
        contact_resf = req2.json()

        return {'scols': contact_scol, 'resf': self.val_or_empty(contact_resf)}

    def num_ordre_not_null(self, val):
        if val:
            return val
        else:
            return 0

    # permit to retrieve contact from maquette
    def get_contact(self, parameters):
        contact_centre = self.get_contact_centre()
        contact_filiere = self.get_contact_filiere(parameters)
        filiere = self.get_filiere_data(parameters)
        val = {'titre': self.part.titre, 'centre': contact_centre, 'filiere': contact_filiere,
               'filiere_r': self.val_or_empty(filiere)}
        return val

    # Obtention dela liste des UE
    def get_list_ue(self, params):
        l_ue = None
        for param in params:
            pass
            # first we get etp tree

            # we iter over tree

            # then by try we get ue of this tree that have date // format {etp:param_etp,code:part_code;examen:mode_title,duration:mod_delta_duration,debut:mod_debut,date:mod_date}

            # we make order bunch of date // format : k,v where k = a date v = list of ue

            # then we return the list

        return l_ue

    def child_ects(self, child, pere):
        try:
            req = requests.get(url=settings.MODULE + str(child['code']))
            res = req.json()
            if res['ects_mult']:
                etp = self.part.parametre.split('-')[0]
                ver = self.part.parametre.split('-')[1]
                for elt in res['ects_mult']:
                    if elt['pere'] == pere['code'] and elt['etape_code'] == etp and elt['etape_version'] == ver:
                        return elt['ects']
                raise Exception('rien trouve')
            else:
                return child['_ects']
        except Exception as e:
            return child['_ects']

    def parse_tree(self, tree, anchor=None, pere=None):
        bunch = []
        res = []
        for child in tree:
            if child['children']:
                res = self.parse_tree(child['children'], anchor, pere=child)
            if child['element_type'] == 'UE' or child['element_type'] == 'SEM' or child['element_type'] == 'MATI' or \
                    child['element_type'] == 'PAR' or child['element_type'] == 'ELC' or child[
                'element_type'] == 'OPT' or child['element_type'] == 'UEST' or child['element_type'] == 'UEPR' or child[
                'element_type'] == 'PRJ' or child['element_type'] == 'STAG' or child['element_type'] == 'CHOI':
                elt = {'lib': child['text'].split('-')[0], 'ects': self.child_ects(child, pere),
                       'type': child['element_type'], 'periode': child['periode'], 'code': child['code'],
                       'num_pre_elp': self.num_ordre_not_null(child['num_ordre']), 'children': res, 'anchor': anchor}
                bunch.append(elt)
            else:
                bunch.extend(res)
            res = []
        return bunch

    def parse_tree_description(self, tree, param, pere=None):
        bunch = []
        res = []
        for child in tree:
            if child['children']:
                res = self.parse_tree_description(child['children'], param)
            if child['element_type'] == 'UE' or child['element_type'] == 'MATI' or child['element_type'] == 'PAR' or \
                    child['element_type'] == 'ELC' or child['element_type'] == 'CHOI' or child[
                'element_type'] == 'OPT' or child['element_type'] == 'UEST' or child['element_type'] == 'UEPR' or child[
                'element_type'] == 'PRJ' or child['element_type'] == 'STAG':
                try:
                    module = Module.objects.get(code=child['code'], annee_module=Annee.objects.get(current=True))
                    elt = {'lib': child['text'].split('-')[0], 'ects': self.child_ects(child, pere),
                           'type': child['element_type'], 'periode': child['periode'], 'code': child['code'],
                           'num_pre_elp': self.num_ordre_not_null(child['num_ordre']), 'children': res,
                           'module': module, 'key': param}
                    bunch.append(elt)
                except Module.DoesNotExist:
                    pass
            else:
                bunch.extend(res)
            res = []
        return bunch

    def find_semestre(self, elts):
        found = 0
        for elt in elts:
            if elt['element_type'] in ['SEM', 'PAR']:
                if found == 0:
                    s1 = elt
                    found += 1
                elif found == 1:
                    s2 = elt
                    found += 1
        if found == 2:
            return s1, s2
        else:
            return elts[0], None

    def process_struct(self, param):
        result = None
        param = param.split('-')
        an = Annee.objects.get(current=True).cod_anu
        req = requests.get(url=settings.TREE + str(param[0]) + '/' + param[1] + '/' + an)
        res = req.json()
        s1, s2 = self.find_semestre(res['children'])
        try:
            if s1['num_ordre'] > s2['num_ordre']:
                k_s1 = s1
                s1 = s2
                s2 = k_s1
        except Exception:
            pass
        if len(s1['children']) == 0:
            s1 = res
            s2 = None
        try:
            anchor = param[0]
        except Exception as e:
            anchor = None
        result = self.parse_tree(s1['children'], anchor, s1)
        if s2:
            result2 = self.parse_tree(s2['children'], anchor, s2)
        else:
            result2 = []
            s2 = {'lib': ''}
            s1.update({'lib': s1['text']})
        res = {'s1': result, 's2': result2, 't1': s1['lib'], 't2': s2['lib'], 'titre': self.part.titre,
               'ancre': self.create_format_ancre(self.part.titre)}
        return res

    def process_struct_module(self, param):
        result = None
        param = param.split('-')
        key = param[0]
        an = Annee.objects.get(current=True).cod_anu
        req = requests.get(url=settings.TREE + str(param[0]) + '/' + param[1] + '/' + an)
        res = req.json()
        s1, s2 = self.find_semestre(res['children'])
        try:
            if s1['num_ordre'] > s2['num_ordre']:
                k_s1 = s1
                s1 = s2
                s2 = k_s1
        except Exception:
            pass
        if len(s1['children']) == 0:
            s1 = res
            s2 = None
        result = self.parse_tree_description(s1['children'], key, s1)
        if s2:
            result2 = self.parse_tree_description(s2['children'], key, s2)
        else:
            result2 = []
            s2 = {'lib': ''}
            s1.update({'lib': s1['text']})
        res = {'s1': result, 's2': result2, 't1': s1['lib'], 't2': s2['lib'], 'titre': self.part.titre,
               'ancre': self.create_format_ancre(self.part.titre)}
        return res

    def create_date_groupe(self, ues):
        dict_res = {}
        tmp_list_date = []
        mods = Module.objects.filter(code__in=ues, annee_module=Annee.objects.get(current=True),
                                     date_debut_examen__isnull=False).order_by('date_debut_examen')
        if mods.count() == 0:
            return dict_res
        old_key = mods.first().date_debut_examen.strftime('%d-%m-%Y')
        for mod in mods:
            try:
                if mod.date_debut_examen.strftime('%d-%m-%Y') == old_key:
                    tmp_list_date.append(mod)
                else:
                    dict_res.update({old_key: tmp_list_date})
                    tmp_list_date = []
                    old_key = mod.date_debut_examen.strftime('%d-%m-%Y')
                    tmp_list_date.append(mod)
            except Exception:
                pass
        dict_res.update({old_key: tmp_list_date})
        return dict_res

    def create_date_groupe_r(self, ues):
        dict_res = {}
        tmp_list_date = []
        mods = Module.objects.filter(code__in=ues, annee_module=Annee.objects.get(current=True),
                                     date_debut_examen_r__isnull=False).order_by('date_debut_examen_r')
        if mods.count() == 0:
            return dict_res
        old_key = mods.first().date_debut_examen_r.strftime('%d-%m-%Y')

        for mod in mods:
            try:
                if mod.date_debut_examen_r.strftime('%d-%m-%Y') == old_key:
                    tmp_list_date.append(mod)
                else:
                    dict_res.update({old_key: tmp_list_date})
                    tmp_list_date = []
                    old_key = mod.date_debut_examen_r.strftime('%d-%m-%Y')
                    tmp_list_date.append(mod)
            except Exception:
                pass
        dict_res.update({old_key: tmp_list_date})
        return dict_res

    def retrieve_exam_data(self, param):
        result = None
        pre_param = param.split(';')  # first we retrieve each Etape-Version parameters then we split again
        an = Annee.objects.get(current=True).cod_anu  # we retrieve current year
        ues = []
        # we iterate over each parameters to get etp tree
        for etv in pre_param:
            param = etv.split('-')
            req = requests.get(url=settings.TREE + str(param[0]) + '/' + param[1] + '/' + an)
            tree = req.json()
            tree_start = tree['children']
            ues_r = self.retrieve_ue_exam(tree_start)
            ues.extend(ues_r)
        res = self.create_date_groupe(ues)
        return res

    def retrieve_exam_data_r(self, param):
        result = None
        pre_param = param.split(';')  # first we retrieve each Etape-Version parameters then we split again
        an = Annee.objects.get(current=True).cod_anu  # we retrieve current year
        ues = []
        # we iterate over each parameters to get etp tree
        for etv in pre_param:
            param = etv.split('-')
            req = requests.get(url=settings.TREE + str(param[0]) + '/' + param[1] + '/' + an)
            tree = req.json()
            tree_start = tree['children']
            ues_r = self.retrieve_ue_exam(tree_start)
            ues.extend(ues_r)
        res = self.create_date_groupe_r(ues)
        return res

    def elt_as_exam(self, elt):
        try:
            req = requests.get(url=settings.MAQ_EXAM + str(elt['code']))
            res = req.json()['as_exam']
            return res
        except Exception:
            return False

    def retrieve_ue_exam(self, tree_start):
        list_ue = []
        for elt in tree_start:
            if elt['children']:
                res = self.retrieve_ue_exam(elt['children'])
                list_ue.extend(res)
            if self.elt_as_exam(elt):  # we append code that should have exam
                list_ue.append(elt['code'])
        return list_ue

    #Génération du code LaTeX à partir du template contact
    def render_contact(self, template_name, part):
        template = loader.get_template(template_name)
        contacts = self.get_contact(part.parametre)
        return template.render(context=contacts)

    # Génération du code LaTeX à partir du template calendrier général
    def render_calendar(self, template_name):
        template = loader.get_template(template_name)
        dates = GeneralCalendar.objects.filter(annee=Annee.objects.get(current=True)).order_by('debut')
        return template.render(context={"dates": dates, 'titre': self.part.titre})

    # Génération du code LaTeX à partir du template calendrier des examens
    def render_calendar_exam(self, template_name):
        first = True
        res = ''
        template = loader.get_template(template_name)
        for param in self.part.parametre.split(','):
            ctx = self.retrieve_exam_data(param)
            ctx2 = self.retrieve_exam_data_r(param)
            if first:
                res = template.render(
                    context={'etp_titre': self.get_etp_titre(param), 'session1': ctx, 'session2': ctx2, 'first': first})
                first = False
            else:
                res += template.render(
                    context={'etp_titre': self.get_etp_titre(param), 'session1': ctx, 'session2': ctx2, 'first': first})
        return res

    # Génération du code LaTeX à partir du template vierge
    def render_template(self, template_name, part):
        template = loader.get_template(template_name)
        return template.render(context=part)

    def get_etp_titre(self, param):
        try:
            etp = param.split('-')[0]
            v_etp = param.split('-')[1]
            req = requests.get(
                url=settings.ETAPE + str(etp) + '/' + str(v_etp) + '/' + str(Annee.objects.get(current=True).cod_anu))
            res = req.json()
            return res[0]['libelle']
        except Exception:
            return ''

    # Génération du code LaTeX à partir du template structure d'une année de filière
    def render_structure(self, template_name):
        template = loader.get_template(template_name)
        ctx = self.process_struct(self.part.parametre)
        return template.render(context=ctx)

    # Génération du code LaTeX à partir du template module
    def render_module(self, template_name):
        template = loader.get_template(template_name)
        ctx = self.process_struct_module(self.part.parametre)
        return template.render(context=ctx)

    # Génération du code LaTeX à partir d'une page vide (utilisé pour le contenu édité sur Charon)
    def render_free_page(self, part):
        template = loader.get_template('page-gabarit.tex')
        ancre = ""
        if len(part.key)==0:
            ancre = self.create_format_ancre(part.titre);
        else :
            ancre = part.key
        return template.render(context={'titre': part.titre, 'ancre': ancre,
                                        'content': self.prepare_text(part.page.contenu_page)})

    # Fonction appelant une génération à partir du type de la partie. Elle permet de renvoyer vers le bon template
    def convert(self):
        # in this case we simply return html
        if self.part.type_parti == 'com' or self.part.type_parti == 'gui':
            return self.render_free_page(self.part)
            return ""
        # Appel du parser correspondant au type de page souhaité
        elif self.part.type_parti == 'gen':
            if self.part.sous_type == 'cfi':
                return self.render_contact("contacts.tex", self.part)
            elif self.part.sous_type == 'cal':
                return self.render_calendar("calendrier_general.tex")
            elif self.part.sous_type == 'exa':
                return self.render_calendar_exam("calendrier_examen.tex")
            elif self.part.sous_type == 'mod':
                return self.render_module("module_generable.tex")
            elif self.part.sous_type == 'str':
                return self.render_structure("structure.tex")
