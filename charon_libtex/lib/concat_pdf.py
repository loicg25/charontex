# librairie de concaténation pdf
from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
import io

"""

Class qui effectue la concaténation PDF

"""


class ConcatPdf(object):
    """

    Fonction qui effectue la concaténation de deux fichier pdf
    :pdf1 Premier fichier pdf qui sera ajouter en premier
    :pdf2 Second fichier pdf qui sera ajouter en second
    :from_to Page de debut du pdf par défaut 0 = le début
    :return Fichier pdf résultat

    """

    @staticmethod
    def concat_pdf(pdf1, pdf2, from_to=0):
        # fichier resultat
        pdf_result = PdfFileWriter()
        # fichier lecteur

        pdf_reader = PdfFileReader(pdf1)
        pdf_reader2 = PdfFileReader(pdf2)

        # for page in range(pdf_reader.getNumPages()):
        #
        #     pdf_result.addPage(pdf_reader.getPage(page))
        #
        # for page in range(int(from_to),pdf_reader2.getNumPages()):
        #
        #     pdf_result.addPage(pdf_reader2.getPage(page))

        pdf_merge = PdfFileMerger()
        pdf_merge.append(pdf1)
        pdf_merge.append(pdf2)

        result_file = io.BytesIO()
        #
        pdf_merge.write(result_file)

        return result_file
