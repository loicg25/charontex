from html.entities import name2codepoint
from html.parser import HTMLParser
import base64
import numpy as np
import htmlmin


class MyHTMLParser(HTMLParser):
    latexOutput = ""  # contient le code latex en sortie du parser
    align = ""  # contient l'alignement à appliquer à l'élément courant en cours de parsing
    tab = []  # contient le contenu des différentes cellules du tableau courant en cours de parsing
    col = 0  # nb colonne dans le tableau courant en cours de parsing
    lign = 0  # nb lignes dans le tableau courant en cours de parsing
    nbcol = []  # tableau contenant le nombre de colonnes de chaque ligne du tableau en cours de parsing
    maxCol = 0  # nombre maximum de colonnes dans le tableau en cours de parsing
    titreExist = False  # titre existant sur le tableau
    jumpingPage = False  # booleen pour savoir si un saut de page est nécéssaire
    coloring = False  # booleen pour savoir si le texte en cours de parsing doit être coloré
    cellToParse = ""  # contient le code de la cellule en cours de construction dans le tableau en cours de parsing, ce contenu va être traité par une autre instance de ce parser
    creatingATabCell = False  # boolean pour signaler qu'une cellule de tableau est en cours de création
    hasAnAlign = False  # boolean pour savoir si le code actuel possède un alignement (centré, droite, justifié)
    cellAlignment = ""  # contient l'alignement à appliquer à la cellule en cours de création
    isAComplexCollCell = False  # booleen indiquant si la cellule fait partie d'une fusion de colonnes
    isAComplexLignCell = False  # booleen indiquant si la cellule fait partie d'une fusion de lignes
    isCentering = False  # booleen pour savoir si le texte doit être centré
    withoutBorder = False  # booleen pour savoir si le tableau possède ou non des bordures
    restRow = 0  # permet de compter les colonnes restant à construire
    creatingLatex = False  # booleen pour savoir si le code parsé actuellement est déjà en LaTeX
    isAncre = False  # booleen pour savoir si le lien courant est une ancre
    inOneCellTab = False  # booleen pour savoir si le tableau courant est un tableau à une seule cellule
    threeMinusColTab = False  # booleen pour savoir si le tableau courant possède moins de 3 colonnes
    fiveBigColTab = False  # booleen pour savoir si le tableau courant possède plus de 5 colonnes
    isSuperTable = False  # booleen pour savoir si le tableau doit être découpé sur plusieurs pages
    oneCellAlignmentBegin = ""  # contient le début du code annoncant l'alignement du tableau à une seule cellule
    oneCellAlignmentEnd = ""  # contient la fin du code contenant l'alignement du tableau à une seule cellule

    # permet d'obtenir le lien des balises html indiquant une redirection
    def getAttrLink(self, attrs):
        if (attrs[0][1] == None):
            return ""
        else:
            return attrs[0][1]

    # permet d'obtenir l'alignement en cours de traitement
    def getStyleAlign(self, att):
        if att == "text-align:center":
            self.align = "center"
            return "\\begin{center}\n"
        elif att == "text-align:right":
            self.align = "flushright"
            return "\\begin{flushright}"
        else:
            return ""

    # permet de définir le code LaTeX à appliquer pour l'alignement d'une cellule de tableau
    def getCellStyleAlign(self, att):
        # alignement au centre
        if att == "text-align:center" or att == "text-align: center" or att == "text-align:center;" or att == "text-align: center;":
            self.hasAnAlign = True
            self.isCentering = True
            self.cellAlignment = "\\begin{center}"
            self.oneCellAlignmentBegin = "\\begin{center}"
            self.oneCellAlignmentEnd = "\\end{center}"
            return "\\begin{center}"

        # alignement à droite
        elif att == "text-align:right" or att == "text-align: right" or att == "text-align:right;" or att == "text-align: right;":
            self.hasAnAlign = True
            self.oneCellAlignmentBegin = "\\begin{flushright}"
            self.oneCellAlignmentEnd = "\\end{flushright}"
            if self.withoutBorder:
                self.cellAlignment = "\CV{}{"
                return "\CV{}{"
            else:
                self.cellAlignment = "\CV|{"
                return "\CV|{"

        # alignement à gauche
        elif att == "text-align:left" or att == "text-align: left" or "text-align:left;" or att == "text-align: left;":
            self.hasAnAlign = False
            self.cellAlignment = ""
            return ""

        # justification du contenu
        elif att == "text-align:justify" or att == "text-align: justify" or att == "text-align:justify;" or att == "text-align: justify;":
            self.hasAnAlign = True
            self.isCentering = True
            self.oneCellAlignmentBegin = "\\begin{center}"
            self.oneCellAlignmentEnd = "\\end{center}"
            self.cellAlignment = "\\begin{center}"
            return "\\begin{center}"

    # permet de generer le code latex pour la coloration de texte en utilisant directement le code couleur HTML
    def getStyleColor(self, att):
        # On supprime toutes les combinaisons possibles du préfixe
        a1 = att.removeprefix('color:#')
        a2 = a1.removeprefix('color:\#')
        a3 = a2.removesuffix(';')
        return '\\textcolor[HTML]{' + a3 + '}{'

    # permet de generer les cellules dites "speciales", autrement dit les fusions de lignes et de colonnes dans un tableau
    def handleSpecialCells(self, tamp):
        for indLig in range(self.lign):
            for indCol in range(self.maxCol):
                # on détecte les fusions de colonnes
                if ("\multicolumn" in str(tamp[indLig][indCol])):
                    # on récupère le nombre de colonnes fusionnées
                    nbMultiCol = (tamp[indLig][indCol].removeprefix("\multicolumn{"))[0]
                    nbMultiCol = int(nbMultiCol)
                    # On marque les colonnes devant être ignorées lors de la génération du tableau car faisant partie de la fusion de colonnes
                    for x in range(1, nbMultiCol - 2):
                        if (indCol + x):
                            # On utilise une chaine de caractère qui ne sera jamais generée autrement que par cette méthode
                            tamp[indLig][indCol + x] = "_-_9898emptySpaceLatexGeneratorNeverHandMade9898_-_"

                # on détecte les fusions de lignes
                elif ("\multirow" in str(tamp[indLig][indCol])):
                    # on récupère le nombre de lignes fusionnées
                    nbMultiRow = (tamp[indLig][indCol].removeprefix("\multirow{"))[0]
                    nbMultiRow = int(nbMultiRow)
                    # On décale le contenu pour vider les cellules qui vont être fusionnées (On ne garde que le contenu de la première cellule fusionnée et on décale le reste)
                    for x in range(1, nbMultiRow):
                        tamp[indLig + x] = np.roll(tamp[indLig + x], self.maxCol - indCol + 1)
                        tamp[indLig + x][indCol] = " "
                        #On indique les cellules marquées comme vides
                if (tamp[indLig][indCol] == 0):
                    tamp[indLig][indCol] = " "
        return tamp

    # permet la génération des tableaux
    def createTable(self):
        res = ""
        # cas de l'alignement dans un tableau à une seule cellule
        if (self.inOneCellTab):
            res += self.tab[0]
            res.replace('\CV|', '')
            res.replace('\CV', '')
            res1 = res.removeprefix('\CV|')
            res2 = res1.removeprefix('\CV')
            return res2

        # boucle principale de création
        self.restRow = 0
        # création d'une matrice répresentant le tableau en cours de création
        tamp = [[0 for x in range(self.maxCol)] for y in range(self.lign)]
        j = 0
        # on parcourt le tableau contenant le contenu des différentes cellules du tableau
        for x in range(0, self.lign):
            for y in range(self.nbcol[x]):
                if (j < len(self.tab)):
                    # on remplit la matrice avec le contenu du tableau en cours de création
                    tamp[x][y] = self.tab[j]
                    j += 1
        # gestion des fusions de cellules
        tamp = self.handleSpecialCells(tamp)
        # gestion des bordures
        if not self.withoutBorder:
            res = "|"
        for i in range(self.maxCol):
            if self.withoutBorder:
                res += "Q\\raggedright m "
            else:
                res += "Q\\raggedright m|"
                # res += "c|"
        if self.withoutBorder:
            res += "}\n"
        else:
            res += "}\hline\n"
        # Création de l'output LaTeX contenant la totalité du code pour generer le tableau
        for x in range(0, self.lign):
            # On génère newline, la variable qui contient une ligne vide qui sera utilisée pour créer un espacement entre les lignes fusionnées
            newLine = "\\\\"
            for y in range(self.maxCol - 1):
                newLine += " & "
            newLine += "\\\\"
            # On détecte les cellules indiquées comme étant à ignorer afin de ne pas les traiter
            for y in range(self.maxCol):
                if not (tamp[x][y] == "_-_9898emptySpaceLatexGeneratorNeverHandMade9898_-_" or tamp[x][y] == " "):
                    # On ajoute à l'output LaTeX le code correspondant à la cellule courante
                    # On détecte si la cellule courante est la première de sa ligne  car le traitement est différent dans LaTeX
                    if y == 0:
                        res += tamp[x][y] + " "
                    else:
                        # si ce n'est pas la première cellule de la ligne, on ajoute & pour l'indiquer à LaTeX
                        res += "& " + tamp[x][y] + " "
            if self.withoutBorder:
                # On gère les fusions de lignes pour l'output LaTeX à l'aide de newline dans le cas d'un tableau sans bordure
                if ("\multirow" in str(tamp[x])):
                    nbMultiRow = (str(tamp[x]).removeprefix("['\\\\multirow{"))[0]
                    nbMultiRow = int(nbMultiRow)
                    self.restRow = nbMultiRow - 2
                    res += newLine

                else:
                    # Traitement identique sans bordure.
                    # On utilise newLine pour ajouter une ligne entre chaque ligne faisant partie de la fusion pour avoir un rendu plus aéré.
                    if self.restRow > 0:
                        res += newLine
                        self.restRow -= 1
                    else:
                        res += "\\\\\n"
                res += "\\\\\n"


            else:
                # On gère les fusions de lignes pour l'output LaTeX à l'aide de newline dans le cas d'un tablleau avec bordure
                if ("\multirow" in str(tamp[x])):
                    nbMultiRow = (str(tamp[x]).removeprefix("['\\\\multirow{"))[0]
                    nbMultiRow = int(nbMultiRow)
                    self.restRow = nbMultiRow - 2
                    res += newLine

                else:
                    # On utilise newLine pour ajouter les lignes faisant partie de la fusion
                    if self.restRow > 0:
                        res += newLine
                        self.restRow -= 1
                    else:
                        res += "\\\\\hline\n"

        # On indique la fin d'un supertableau si le tableau courant est de ce type (sur plusieurs pages)
        if self.isSuperTable:
            res += "\end{supertabular}\n\n"
            self.isSuperTable = False
        else:
            # Sinon on termine le tableau de manière conventionnelle en LaTeX
            res += "\end{tabular}\n\end{table}\n"
        return res

    # permet de créer le rendu d'une image en base 64 initialement
    def createImg64(self, att):
        from datetime import datetime
        filename = "img_" + str(datetime.now()) + ".png"
        filename_whitoutSpace = filename.replace(" ", "")
        filename_ok = filename_whitoutSpace.replace(":", "")
        codeBase64 = att.removeprefix('data:image/png;base64,')
        imgdata = base64.b64decode(codeBase64)
        path = "generated_docs/"
        with open(path + filename_ok, 'wb') as f:
            f.write(imgdata)
        # on retourne le nom de l'image pour l'appeler dans le code LaTeX
        return filename_ok

    # permet de proteger les caractères spéciaux en LaTeX pour la bonne compilation du code final
    def protectAndUnescapeText(self, text):
        protectB = text.replace("\b", "\\b")
        protectT = protectB.replace("\t", "\\t")
        protectN = protectT.replace("\n", "\\n")
        protectR = protectN.replace("\r", "\\r")
        protectF = protectR.replace("\f", "\\f")
        protectEsper = protectF.replace("&", "\&")
        protectPourc = protectEsper.replace("%", "\%")
        protectDie = protectPourc.replace("#", "\#")
        protectUnde = protectDie.replace("_", "\_")
        protectDollar = protectUnde.replace("$", "\$")

        return protectDollar

    # parsing des balises ouvrantes en HTML et ajout du code correspondant en LaTeX dans la variable d'output + modif des valeurs des booleens décrivant les actions en cours
    # Si on est en train de créer une cellule de tableau, on ajoute le code dans cellToParse afin de nourrir une deuxième instance du parser pour appliquer les styles etc;
    # Sinon, on ajoute le code dans l'output du parser courant
    def handle_starttag(self, tag, attrs, ):
        # Gestion des sous-sections
        if tag == "h2":
            self.latexOutput += "\subsection{"

        # Gestion des sous sous-sections
        elif tag == "h3":
            self.latexOutput += "\subsubsection{"

        # Début de liste non numéroté
        elif tag == "ul":
            if (self.creatingATabCell):
                self.cellToParse += "\\begin{itemize}\n"
            else:
                self.latexOutput += "\\begin{itemize}\n"

        # Element d'une liste
        elif tag == "li":
            if (self.creatingATabCell):
                self.cellToParse += "\item "
            else:
                self.latexOutput += "\item "

        # Gestion des liens
        elif tag == "a":
            # récupération du lien de la balise <a>
            link = self.getAttrLink(attrs)
            if link == '\#':
                name = ""
                for attr in attrs:
                    if attr[0] == "name":
                        name = attr[1]
                # prise en compte du style des liens dans les tableaux
                if self.creatingATabCell:
                    self.cellToParse += "\hyperref[" + name + "]{\\textcolor{blue}{\\underline{"
                else:
                    self.latexOutput += "\hyperref[" + name + "]{"
            # Cas où le lien est une ancre
            elif len(link) > 1 and link[0] == '#':
                if self.creatingATabCell:
                    self.isAncre = True
                    self.cellToParse += "\phantomsection\label{" + link[1:] + "}{"
                else:
                    self.latexOutput += "\phantomsection\label{" + link[1:] + "}{"
            else:
                if self.creatingATabCell:
                    self.cellToParse += "\href{" + self.getAttrLink(attrs) + "}{\\textcolor{blue}{\\underline{"
                else:
                    self.latexOutput += "\href{" + self.getAttrLink(attrs) + "}{"

        # Décoration de texte : souligné
        elif tag == "u":
            if (self.creatingATabCell):
                self.cellToParse += "\\underline{"
            else:
                self.latexOutput += "\\underline{"

        # Décoration de texte : Gras
        elif tag == "strong":
            if (self.creatingATabCell):
                self.cellToParse += "{\\bfseries "
            else:
                self.latexOutput += "{\\bfseries "

        # Décoration de texte : italique
        elif tag == "em":
            if (self.creatingATabCell):
                self.cellToParse += "{\\itshape "
            else:
                self.latexOutput += "{\\itshape "

        # Mise en indice
        elif tag == "sub":
            if (self.creatingATabCell):
                self.cellToParse += "\\textsubscript{"
            else:
                self.latexOutput += "\\textsubscript{"

        # Mise en exposant
        elif tag == "sup":
            if (self.creatingATabCell):
                self.cellToParse += "\\textsuperscript{"
            else:
                self.latexOutput += "\\textsuperscript{"

        # Début de liste numéroté
        elif tag == "ol":
            if (self.creatingATabCell):
                self.cellToParse += "\\begin{enumerate}\n"
            else:
                self.latexOutput += "\\begin{enumerate}\n"

        # Gestion des différents types de balise <p>
        elif tag == "p":
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == "style":
                        # détection d'un besoin d'alignement
                        if (self.creatingATabCell):
                            self.cellToParse += self.getStyleAlign(attr[1])
                        else:
                            self.latexOutput += self.getStyleAlign(attr[1])

        # Gestion des différents types de span <p>
        elif tag == "span":
            if len(attrs) > 0:
                for attr in attrs:
                    # détection d'un besoin de style
                    if attr[0] == "style":
                        if not self.jumpingPage:
                            # détection d'un besoin de coloration
                            if "color" in attr[1]:
                                self.coloring = True
                                if (self.creatingATabCell):
                                    self.cellToParse += self.getStyleColor(attr[1])
                                else:
                                    self.latexOutput += self.getStyleColor(attr[1])

                    # détection de code LaTeX fourni à l'aide du plugin de ckeditor
                    elif attr[0] == "class":
                        self.creatingLatex = True

        # Début d'un tableau à generer
        elif tag == "table":
            self.maxCol = 0
            self.col = 0
            self.tab = []
            self.nbcol = []
            self.lign = 0
            if len(attrs) > 0:
                # détection de la présence ou non de bordures
                for attr in attrs:
                    if attr[0] == "border":
                        if attr[1] == "0":
                            self.withoutBorder = True
                            self.latexOutput += "\\renewcommand\CV{}\def\CV#1{\multicolumn1{#1Q\\raggedleft m}}\n\n"
                        else:
                            self.latexOutput += "\\renewcommand\CV{}\def\CV#1{\multicolumn1{#1Q\\raggedleft m|}}\n\n"

        # Légende du tableau
        elif tag == "caption":
            self.latexOutput += "\caption{{\\itshape "
            self.titreExist = True

        # Contenu du tableau
        elif tag == "tbody":
            if (self.titreExist):
                self.titreExist = False
            self.inTable = True

        # Ligne d'en-tête du tableau (titre des colonnes)
        elif tag == "th":
            self.creatingATabCell = True
            self.cellToParse = "{\\bfseries "

        # détection d'une nouvelle colonne (donc d'une nouvelle cellule)
        elif tag == "td":
            self.hasAnAlign = False
            self.creatingATabCell = True
            self.cellToParse = ""
            # détection du besoin de style à appliquer à la cellule courante
            if len(attrs) > 0:
                for attr in attrs:
                    # détection du besoin d'alignement pour la cellule courante
                    if attr[0] == "style":
                        ats = attr[1].split('; ')
                        for at in ats:
                            if ("text-align:" in at):
                                self.cellToParse += self.getCellStyleAlign(at)
                    # détection d'une fusion de colonnes
                    if attr[0] == "colspan":
                        # récupération du nombre de colonnes à fusionner
                        if (int(attr[1]) > 1):
                            self.isAComplexCollCell = True
                            if self.withoutBorder:
                                self.cellToParse += '\multicolumn{' + attr[1] + '}{c'
                            else:
                                self.cellToParse += '\multicolumn{' + attr[1] + '}{|c|'
                            self.cellToParse += '}{'
                    # détection d'une fusion de lignes
                    if attr[0] == "rowspan":
                        # récupération du nombre de lignes à fusionner
                        if (int(attr[1]) > 1):
                            self.isAComplexLignCell = True
                            self.cellToParse += "\multirow{" + attr[1] + "}{*}{"


        # Ligne d'un tableau
        elif tag == "tr":
            self.col = 0

        # Balise div permettant de détecter une demande de nouvelle page
        elif tag == "div":
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == "style":
                        # On indique un saut de page après la section
                        if attr[1] == "page-break-after:always":
                            self.jumpingPage = True
                            self.latexOutput += "\\newpage\n"

        # Gestion des images
        elif tag == "img":
            if len(attrs) > 0:
                for attr in attrs:
                    # récupération du code de l'image en b64
                    if attr[0] == "src":
                        if "data:image/png;base64" in attr[1]:
                            # génération de l'image pour enregistrement
                            name = self.createImg64(attr[1])
                            self.latexOutput += "\includegraphics[scale=0.60]{{./" + name + "}"

        # Saut de ligne
        elif tag == "br":
            if (self.creatingATabCell):
                self.cellToParse += "\n\n"
            else:
                self.latexOutput += "\\\\"

    # parsing des balises fermantes en HTML et ajout du code correspondant en LaTeX dans la variable d'output + modif des valeurs des booleens décrivant les actions en cours
    # Si on est en train de créer une cellule de tableau, on ajoute le code dans cellToParse afin de nourrir une deuxième instance du parser pour appliquer les styles etc;
    # Sinon, on ajoute le code dans l'output du parser courant
    def handle_endtag(self, tag):
        # Gestion des sous-sections
        if tag == "h2":
            self.latexOutput += "}\n\n"

        # Gestion des sous sous-sections
        elif tag == "h3":
            self.latexOutput += "}\n\n"

        # Fin de liste non numéroté
        elif tag == "ul":
            if (self.creatingATabCell):
                self.cellToParse += "\end{itemize}\n"
            else:
                self.latexOutput += "\end{itemize}\n"

        # Fin de l'element d'une liste
        elif tag == "li":
            if (self.creatingATabCell):
                self.cellToParse += "\n"
            else:
                self.latexOutput += "\n"

        # Fin liens
        elif tag == "a":
            if self.creatingATabCell:
                if self.isAncre:
                    self.cellToParse += "}"
                    # reset du booleen des ancres
                    self.isAncre = False
                else:
                    self.cellToParse += "}}}"
            else:
                self.latexOutput += "}"

        # Fin de texte souligné
        elif tag == "u":
            if (self.creatingATabCell):
                self.cellToParse += "}"
            else:
                self.latexOutput += "}"

        # Fin de texte en gras
        elif tag == "strong":
            if (self.creatingATabCell):
                self.cellToParse += "}"
            else:
                self.latexOutput += "}"

        # Fin de texte en italique
        elif tag == "em":
            if (self.creatingATabCell):
                self.cellToParse += "}"
            else:
                self.latexOutput += "}"

        # Fin de l'element en indice
        elif tag == "sub":
            if (self.creatingATabCell):
                self.cellToParse += "}"
            else:
                self.latexOutput += "}"

        # Fin de l'element en exposant
        elif tag == "sup":
            if (self.creatingATabCell):
                self.cellToParse += "}"
            else:
                self.latexOutput += "}"

        # Fin de liste numéroté
        elif tag == "ol":
            if (self.creatingATabCell):
                self.cellToParse += '\end{enumerate}\n'
            else:
                self.latexOutput += '\end{enumerate}\n'

        # Fin balise <p>
        elif tag == "p":
            # On vérifie si un alignement est en attente d'application. Si c'est le cas, on l'ajoute
            if self.align != "":
                if self.creatingATabCell:
                    self.cellToParse += "\n\end{" + self.align + "}"
                    self.align = ""
                else:
                    self.latexOutput += "\n\end{" + self.align + "}"
                    self.align = ""
            if (not self.creatingATabCell):
                self.latexOutput += '\n\n'

        # Fin balise <span>
        elif tag == "span":
            if not self.jumpingPage:
                if self.coloring:
                    self.coloring = False
                    if (self.creatingATabCell):
                        self.cellToParse += "}"
                    else:
                        self.latexOutput += '}'
                if self.creatingLatex:
                    self.creatingLatex = False

        # Fin de ligne du tableau
        elif tag == "tr":
            # mise à jour du nombre maximal de colonnes rencontrées dans une même ligne
            if (self.col > self.maxCol):
                self.maxCol = self.col
            # mise à jour du tableau contenant les nombres de colonnes dans les lignes du tableau
            self.nbcol.append(self.col)
            # incrémentation du nombre de lignes du tableau rencontrées
            self.lign += 1

        # Fin de ligne d'en-tête du tableau
        elif tag == "th":
            # incrémentation du nombre de colonnes de la ligne courante
            self.col += 1
            self.creatingATabCell = False
            self.cellToParse += "}"
            # ajout de l'output du second parser au tableau contenant le contenu des différentes cellules du tableau en cours de génération
            self.tab.append(self.cellToParse)

        # Fin de la cellule
        elif tag == "td":
            # incrémentation du nombre de colonnes de la ligne courante
            self.creatingATabCell = False
            self.col += 1
            # Gestion de l'alignement de la cellule
            if (self.hasAnAlign):
                if (self.isCentering):
                    self.cellToParse += "\end{center}"
                    # reset du booleen
                    self.isCentering = False
                else:
                    self.cellToParse += "}"
            # Gestion de la fusion de colonnes
            if self.isAComplexCollCell:
                self.cellToParse += '}'
                # reset du booleen
                self.isAComplexCollCell = False
                if ((len(self.tab) + 1) % (self.lign + 1) == self.col):
                    if self.withoutBorder:
                        self.cellToParse += '}'
            # Gestion de la fusion de lignes
            if self.isAComplexLignCell:
                self.cellToParse += "}  \n"
                # reset du booleen
                self.isAComplexLignCell = False
            # ajout de l'output du second parser au tableau contenant le contenu des différentes cellules du tableau en cours de génération
            self.tab.append(self.cellToParse)

        # Fin du tableau
        elif tag == "table":
            # Gestion de la largeur des cellules en fonction du nombre de colonnes max du tableau pour un rendu optimal
            if (self.maxCol <= 3):
                # passage du booleen à True pour pouvoir ajuster la largeur des cellules
                self.threeMinusColTab = True
                self.latexOutput += "\\renewcommand\largeur{0.3\\textwidth}\n\n"
            if (self.maxCol >= 5):
                # passage du booleen à True pour pouvoir ajuster la largeur des cellules
                self.fiveBigColTab = True
                self.latexOutput += "\\renewcommand\largeur{0.17\\textwidth}\n\n"
            # Cas d'un tableau à une seule cellule
            if (self.maxCol == 1 and self.lign == 1):
                self.inOneCellTab = True
                self.latexOutput += "\n\\begin{center}\\fbox{\\begin{minipage}{0.7\\textwidth}\\begin{center}"
                if self.hasAnAlign:
                    self.latexOutput += self.oneCellAlignmentBegin
            # Cas d'un tableau nécéssitant plusieurs pages pour être affiché
            if (self.lign > 50):
                self.latexOutput = "\\begin{supertabular}{"
                self.isSuperTable = True
            # Cas d'un tableau de taille normale
            if (not self.isSuperTable and not self.inOneCellTab):
                self.latexOutput += "\\begin{table}[!ht]\n\centering\n"
                if (self.inTable):
                    self.latexOutput += "\\begin{tabular}{"
            # Création du code latex correspondant au tableau courant
            self.latexOutput += self.createTable()
            # fermeture du code LaTex pour un tableau à une seule cellule
            if (self.inOneCellTab):
                self.inOneCellTab = False
                self.latexOutput += self.oneCellAlignmentEnd
                self.latexOutput += "\end{center}\end{minipage}}\end{center}\\newline \\\\"
            # remise de la largeur des cellules à sa valeur par défaut si cette dernière a été modifiée pour ne pas alterer le rendu des prochains tableaux
            if (self.threeMinusColTab or self.fiveBigColTab):
                self.latexOutput += "\\renewcommand\largeur{0.2\\textwidth}\n\n"
                # reset des booleens
                self.threeMinusColTab = False
                self.fiveBigColTab = False
            # Reset des variables utilisées pour la génération d'un tableau afin de ne pas alterer le rendu des prochains tableaux
            self.col = self.lign = 0
            self.inTable = False
            self.withoutBorder = False;
            self.tab = []

        # Fin de la légende du tableau
        elif tag == "caption":
            self.latexOutput += "}}\n"

        elif tag == "div":
            self.jumpingPage = False

    # Gestion des données présentes entre des balises HTML
    def handle_data(self, data):
        # Cas de code LaTeX fourni via le plugin de ckeditor
        if (self.creatingLatex):
            newStr = self.protectAndUnescapeText(data)
        else:
            accFermee = data.replace("}", "\}")
            accOuv = accFermee.replace("{", "\{")
            newStr = accOuv.replace("\t", "")
        # Cas de création de tableau : on ajoute le contenu à la variable qui va être donnée à la seconde instance du parser pour être traité par cette dernière
        if (self.creatingATabCell):
            if (len(newStr) > 1):
                self.cellToParse += newStr
            else:
                if (newStr.isspace()):
                    self.cellToParse += "\hspace{1cm}"
                else:
                    self.cellToParse += newStr

        # Cas par défaut (pas de tableaux en cours de construction) : on ajoute le contenu à l'output Latex du parser si ce dernier à une longueur non nulle pour éviter les espaces qui peuvent provoquer des erreurs LaTeX
        else:
            if (len(newStr) > 0):
                self.latexOutput += newStr

    # permet d'obtenir l'output du parser courant
    def getOutput(self):
        return self.latexOutput

